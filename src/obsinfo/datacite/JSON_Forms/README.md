# Overview

This directory contains the essential files for making forms using JSON Forms (jsonforms.io):

- datacite_pi.json: The schema for Principal Investigators
- datacite_pi.uischema: Tells JSON Forms how to format/display the schema
- App.tsx: A JSON Forms file giving overall structure to the webpage

# Installation

I made this work by installing JSON Forms and putting these files in it's src/ directory.  The installation steps were:

- Install the seed app for React (a webform doohickey)
	- `git clone https://github.com/eclipsesource/jsonforms-react-seed.git`
	- makes a local directory called `jsonforms-react-seed`
- install Node: a Javascript-based environment used to create web servers
  and networked applications
    - `brew install node` installs command-line tools node and npm.
- Go into the `jsonforms-react-seed` directory (i`n ~/soft`
- run `npm ci` from inside the directory to clean install the prerequisites
  (uses `package-lock.json`)
- Copy `datacite_pi.json` and `datacite_pi.uischema.json` to the `src\` directory
- Replace 'Apps.tsx' in the `src/` directory by my version
	- Changes the name of the schema and UI schema files to use,
	  plus changes the page title and makes the form full-width
- Execute `npm start` to start the application
- Browse to http://localhost:3000 to see the application in action

