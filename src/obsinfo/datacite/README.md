I looked at three form makers:
- react-jsonschema-form
- JSON forms
- uniforms

react* is most popular/used, followed by uniforms.  But uniforms is a general
(not json-schema-specific) tool, and json-forms seems to allow the most interface
customization.

# json-forms (jsonforms.io)

Creates customizable forms from a json schema plus a form schema

I am using this for now (subdirectory).  Funnily, it installs it's example code
in a directory called `jsonform-react-seed`

# react-jsonschema-form

creates a form directly from a json schema, and allows
some customization.  The datacite.schema.json in this directory creates a
valid form using react-jsonschema-form.

I was able to make a form using their online example viewer and a barely-modified
datecite.schema.json

But the forms output were purely vertical and I didn't see a way to change that.
   
   
# uniforms (uniforms.tools)

I didn't look at this.
