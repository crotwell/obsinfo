{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Sensor",
    "description": "Sensor Instrument Components",
    "type": "object",
    "required": ["format_version", "sensor_base"],
    "properties": {
        "format_version": {"$ref" : "definitions.schema.json#/format_version"},
        "revision" :      {"$ref": "definitions.schema.json#/revision"},
        "yaml_anchors":   {"$ref": "definitions.schema.json#/yaml_anchors"},
        "notes" :         {"$ref": "definitions.schema.json#/note_list"},
        "sensor_base":    {"$ref": "#/definitions/base" }
    },
    "additionalProperties" : false,
    "definitions": {
        "common_properties": {
            "description": "list of common properties in {element}, modifications, base, definition, and configuration_definition.  This is unused because 'allOf' for makes ugly schema validation",
            "properties": {
                "notes":               {"$ref": "definitions.schema.json#/note_list"},
                "equipment":           {"$ref": "definitions.schema.json#/equipment"},
                "stages":              {"$ref": "stages.schema.json#/definitions/stages" },
                "stage_modifications": {"$ref": "stages.schema.json#/definitions/stage_modifications" },
                "seed_codes":          {"$ref": "#/definitions/seed_codes"}
            }
        },
        "sensor" : {
            "description" : "sensor definition (base+configuration)",
            "type": "object",
            "required": ["base"],
            "properties": {
                "base":                {"$ref" : "#/definitions/base" },
                "configuration":       {"type": ["string", "null"]},
                "modifications":       {"$ref": "#/definitions/modifications"},
                "stage_modifications": {"$ref": "stages.schema.json#/definitions/stage_modifications" },
                "notes":               {"$ref": "definitions.schema.json#/note_list"},
                
                "serial_number":       {"type": "string"}                
            }
        },
        "modifications" : {
            "description" : "sensor modifications: same as definition except base not required",
            "type": "object",
            "properties": {
                "base":                    {"$ref" : "#/definitions/base" },
                "configuration":           {"type": ["string", "null"]},
                "serial_number":           {"type": "string"}, 
                "equipment_modifications": {"$ref": "definitions.schema.json#/equipment"},
                
                "notes":                   {"$ref": "definitions.schema.json#/note_list"},
                "equipment":               {"$ref": "definitions.schema.json#/equipment"},
                "stages":                  {"$ref": "stages.schema.json#/definitions/stages" },
                "stage_modifications":     {"$ref": "stages.schema.json#/definitions/stage_modifications" },
                "seed_codes":              {"$ref": "#/definitions/seed_codes"}
            },
            "additionalProperties" : false
        },
        "base": { 
            "description": "Sensor specification",
            "required": ["equipment",
                         "seed_codes",
                         "stages"],
            "properties": {
                "configuration_default": {"type": "string"},
                "configurations":        {"$ref": "#/definitions/configurations_map"},
                
                "notes":                 {"$ref": "definitions.schema.json#/note_list"},
                "equipment":             {"$ref": "definitions.schema.json#/equipment"},
                "stages":                {"$ref": "stages.schema.json#/definitions/stages" },
                "stage_modifications":   {"$ref": "stages.schema.json#/definitions/stage_modifications" },
                "seed_codes":            {"$ref": "#/definitions/seed_codes"}
            },
            "additionalProperties": false
        },
        "configurations_map": {
            "description": "Map of configuration names",
            "patternProperties": {
                "^[A-Za-z0-9_-]+$": {"$ref": "#/definitions/configuration_definition"}
            }
        },
        "configuration_definition" : {
            "description": "Configuration-specific properties",
            "properties": {
                "configuration_description": {"type": "string"},

                "notes":                     {"$ref": "definitions.schema.json#/note_list"},
                "equipment":                 {"$ref": "definitions.schema.json#/equipment"},
                "stages":                    {"$ref": "stages.schema.json#/definitions/modifications" },
                "stage_modifications":       {"$ref": "stages.schema.json#/definitions/stage_modifications" },
                "seed_codes":                {"$ref": "#/definitions/seed_codes"}
            },
            "additionalProperties": false
        },
        "seed_codes" : {
            "description": "Seed codes, azimuths and dips associated with a sensor",
            "type" : "object",
            "required" : ["band_base",
                          "instrument"],
            "properties" :  {
                "band_base":   {"$ref": "#/definitions/seed_band_base" },
                "instrument":  {"$ref": "#/definitions/seed_instrument_code" }
            },
            "additionalProperties": false
        },
        "seed_band_base" : {
            "type": "string",
            "description": "B for corner period >= 10s, S for <10s (output band code will be adjusted to the sample rate)",
            "enum": ["B","S"]
        },
        "seed_instrument_code": {
            "description": "SEED instrument code",
            "type": "string",
            "pattern": "^[A-Z]$"
        },
        "seed_orientations_map": {
            "description": "permitted orientation codes and their azimuth and dip",
            "type": "object",
            "patternProperties": {
                "^[A-Z0-9]$": {
                    "type": "object",
                    "required": ["azimuth.deg", "dip.deg"],
                    "properties": {
                        "azimuth.deg": {"$ref": "definitions.schema.json#/azimuth.deg"},
                        "dip.deg":     {"$ref": "definitions.schema.json#/dip.deg"}
                    }
                }
            }
        }
    }
}
