# Obsinfo Training Course
Sponsored by ENVRI-FAIR and EPOS SP

## Introducing  obsinfo
- Philosophy and comparison to other systems
- File formats
- Structural units
- Configurations, modifications and shortcuts
- Outputs
    - StationXML files
    - Processing pathways

## Setting up
- Installation
- Copying an example database into your own folder
- Creating StationXML file using the example database
    - Using an example network file
    - Using your own network file

## Creating a StationXML file using your own instruments/deployments
- Creating your own network file (using the example instruments)
- Adding your own sensor/datalogger/analog filter
- Adding your own instrument
- Putting it all together

## “Advanced” issues
- Creating a processing pathway
- Storing and accessing your instrument database online

## Future changes
- Validation of differences from StationXML
	- Only one "equipment" at each level (makes easier to modify)
	- `person:name` instead of `names`

