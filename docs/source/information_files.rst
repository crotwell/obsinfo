*****************
Information files
*****************

Here are examples of information files, from the most basic to the most
complete.  You can also see the schemas at ...

.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
  
  information_files/1_overview
  information_files/2_base_config
  information_files/3_compare_stationxml
  information_files/4_compare_arol
  information_files/examples
