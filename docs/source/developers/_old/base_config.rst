*********************
Base-Configuration
*********************

Along with channel modifications, the base-configuration nomenclature is
at the core of customizing instrumentation in obsinfo.
The philosophical details should be in the user-end documentation, here
are the programming consequences

Esssentially, when a class has a base-configuration nomenclature, calls
to `ObsMetaData.get_configured_element()` are replaced by identical calls
to `ObsMetaData.base_configured_element()`.  The latter method evaluates
the `base:` `configuration:` structure and replaces values as appropriate
before handing off to `ObsMetaData.get_configured_element()`

`base_configured_element()` should, in order:
  1. Check if the configuration has been updated
  2. return the given configuration
  3. Apply local (base-config) changes to the configuration
  4. Apply higher-level (channel-mods?) changes to the configuration
  
Classes using base-configure:
=============================

- `stage`

`timing` uses a simplified version of base-config (without configuration),
allowing one to specify common values in the base and specific ones afterwards



YAML structure: 
=============================

.. code-block:: yaml

    {element}:
        base:
            {element}_property1
            {element}_property2
            {element}_property3
            ...
            configuration_default: <str>
            configuration_definitions:
                {CONFIG_NAME1}:
                    (configuration_description): <str>
                    {element}_propertyN
                    ...
                {CONFIG_NAME2}:
                    (configuration_description): <str>
                    {element}_propertyM
                    ...
                ...
        configuration: <str>
        {element}_propertyY
        ...
        {element_specific_modifier1}  
        ...      



Implementation in schemas
=============================

Every element that uses the base-config nomenclature has the following
element declarations:

+----------------------------+---------------------------------+------------+
| Name                       | properties                      | required   |
=============================+=================================+============+
| `base`                     | - *{properties}*                |            |
|                            | - `configuration_default`       | specified  |
|                            | - `configuration_definitions`   | properties |
+----------------------------+---------------------------------+------------+
| `{element}`                | - `base`                        | base       |
|                            | - `configuration`               |            |
|                            | - *{properties}*                |            |
+----------------------------+---------------------------------+------------+
| `modifications`            | - `base`                        | none       |
|                            | - `configuration`               |            |
|                            | - *{properties}*                |            |
+----------------------------+---------------------------------+------------+
| `configurations_map`       | map of configuration names      | NA         |
|                            | (=> `configuration_definition`) |            |
+----------------------------+---------------------------------+------------+
| `configuration_definition` | - *{properties}*                | none       |
|                            | - `configuration_description`   |            |
+----------------------------+---------------------------------+------------+

there is also a `base_properties` element that lists all of the properties
in a base element.  Originally this was used with `allOf` to avoid repetition,
but `allOf` validation errors are impossible to read so I explicitly state
properties in each type.  The `base_properties` element is therefore just a reference.
In each of the other elements, I separate the base_properties from the element-specific
properties by a blank line, for clarity.

and the hierarchy is:

    {element} > {element}_config > {element}_base
            
That is, any property declared in {element}_config (and "called" using
the "configuration" keyword) will overwrite any property declared
in {element_base}.  Any property in {element} will overwrite any property
declared in the configured {element_base}
