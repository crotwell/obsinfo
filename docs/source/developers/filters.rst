**************************
Filters
**************************


All of the following classes are subclsses of FilterTemplate:
 - Coefficients
 - FIR
 - PolesZeros
 - ResponseList
 - Polynomial
 - ADConversion
 - Analog
 - Digital

They are NOT subclasses of Filter, which exists only to return one of the 
FilterTemplate subclasses.  I therfore use Filter.construct() rather than
Filter()

Filter.construct figures out which filter subclass to create, creates it and
returns it.

The different filter types can be created using Filter.construct().
I think I do this to that we get back an object of one of the Filter
subclasses (PoleZeros, )