****************
Introduction
****************

#########################
Python architecture
#########################

Executables
=============

The following command-line executables perform the main tasks:

- ``makeSTATIONXML``: generates StationXML files from a network +
  instrumentation information files

.. code-block::

 $ python3 makeStationXML -h

displays all the options of makeStationXML.

To create a StationXML file from a file called <filename>, type:

.. code-block::

 $ python3 makeStationXML.py [options] filename 


- ``obsinfo-validate``: validates an information file against its schema
- ``obsinfo-print``: prints a summary of an information file

The following command-line executables make scripts to run specific data conversion software:

- ``obsinfo-make_LCHEAPO_scripts``: Makes scripts to convert LCHEAPO data to miniSEED
- ``obsinfo-make_SDPCHAIN_scripts``: Makes scripts to drift correct miniSEED data and package
  them for FDSN-compatible data centers

Package and Modules
=====================

The package name is ``obsinfo``

``obsinfo.main`` contains code to initialize the main obsinfo routine, to read and potentially validate main (network) information file and to write StationXML file

``obsinfo.network`` and  ``obsinfo.instrumentation`` contain the main code to process the corresponding
information files.

``obsinfo.OBSMetadata`` contains code to read and validate information files in either YAML or JSON formats.

``obsinfo.misc`` contains miscellaneous code, currently deprecated and unsupported, which is not used anymore in the application

``obspy.addons`` contains modules specific to proprietary systems:

- ``obspy.addons.LCHEAPO`` creates scripts to convert LCHEAPO OBS data to
  miniSEED using the ``lc2ms`` software
- ``obspy.addons.SDPCHAIN`` creates scripts to convert basic miniSEED data
  to OBS-aware miniSEED using the ``SDPCHAIN`` software suite
- ``obspy.addons.OCA`` creates JSON metadata in a format used by the
  Observatoire de la Cote d'Azur to create StationXML


Auxiliary subdirectories
===========================

`obsinfo/data/schema`
------------------------------------------------------------

``data/schema`` contains JSON Schema for each file type.


`obsinfo/_examples/`
------------------------------------------------------------

Contains example information files and scripts:

- ``_examples/Information_Files`` contains a complete set of information files

  * ``_examples/Information_Files/network`` contains **network**  files

  * ``_examples/Information_Files/instrumentation`` contains **instrumentation**,
    **instrument_components**, **response** and **filter** files.

- ``_examples/scripts`` contains bash scripts to look at and manipulate these files
  using the executables.  Running these scripts is a good way to make sure your
  installation works, looking at the files they work on is a good way to start
  making your own information files.

`obsinfo/tests/`
------------------------------------------------------------

Contains test cases and code using ``unittest.py``. The tests are performed either on the information files under ``test/data`` or on ``_examples``.

Comments on versioning
=======================

We use standard MAJOR.MINOR.MAINTENANCE version numbering but, while the
system is in prerelease:

- MAJOR==0

- MINOR increments every time the information 
  file structure changes in a **non-backwards-compatible** way

- MAINTENANCE increments when the code changes or the file structure changes
  in a **backwards-compatible** way