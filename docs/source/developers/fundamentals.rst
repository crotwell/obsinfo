****************
Fundamentals
****************

This is a stub.

Explain here the fundamentals of obsinfo coding:
    - Need to follow StationXML as much as possible
        - But to add other fields
        - And to eliminate redundancy
    - Correlation and non-correlation of classes to StationXML objects
      and why
    - How an obsinfo file is parsed to obtain an obspy **Network** object
        - Including non-standard field stuffing into comments.
        
__str__()
====================================

*explain how to write __str__(self, indent=0, n_subclasses=0)*

Verify reading of attributes_dict
====================================

values in attributes_dicts should be "popped" and the final attributes_dict
verified empty to be sure that all inputs are processed (mostly a debugging
process).

For now I'm just doing so on a class-by-class basis, should I write a helper
function to do it everywhere (might have to be able to say where it was
called from in case of error).

Wrote a helper function to verify that a dictionary is empty and, if not, to
state in which calling function it was not.

For example:

```python
    def __init__(self, attributes_dict, higher_modifs={}):
        self.equipment = Equipment(base_dict.pop('equipment', None))
        self.configuration = base_dict.pop('configuration', None)
        self.configuration_description = base_dict.pop('configuration_description', self.configuration)
        seed_dict = self.base_dict.pop('seed_codes', {})
        self.seed_band_base_code = seed_dict.get('band_base', None)
        self.seed_instrument_code = seed_dict.get('instrument', None)
        self._clear_base_dict()
```

```python
    def _clear_base_dict(self):
        if len(self.base_dict) > 0:
            raise ValueError('base_dict has remaining keys: {}'
                             .format(list(self.base_dict.keys())))
        del self.base_dict
```
