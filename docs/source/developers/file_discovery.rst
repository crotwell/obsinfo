****************
File discovery
****************

information files can import other information files using
the `$ref` operator.
This is based on the `jsonref` standard, but we have expanded it to
also work on YAML_ files and to allow several directories in which
to search for the given filepaths.  This is called the `datapath` and
includes the files and urls specified in the ~/.obsinfo file.

The .obsinforc file is created in `main/setupObsinfo.py` and is read
by `Datapath` objects through a call to `ObsinfoConfiguration.datapath`

I would also like to be able to search in the current file's directory,
but haven't yet been able to figure out how, although this was probably
the origina jsonRef default.

I think that the subfiles are read through the recursive
`JsonRef.replace_refs()` classmethod, which is called by
`yamlref.load()`, `yamlref.loads()` and `yamlref.load_uri()`.

Searching for `datapath` in `yamlref.py` suggests that its methods
are only called in the derived property `JsonRef.full_uri` (see below),
which is
called by the `callback` method.  The `callback` method appears to be
intrinsic to the "proxytypes" superclass, but I don't understand it.
According to python documents, it is called when its object is garbage
collected???

One could possibly be inspired by the `jsonschema is True` case,
because jsonschema always reads from the same directory.  On the other
hand, perhaps this directory is passed in the `base_uri` argument
for jsonschema and not for the others?


```python
    @property
    def full_uri(self):
        """
        This method/property returns the full uri to reference a ``$ref`` object.
        It's the heart of how a datapath is used to either access a local or remote (gitlab) file.
        All schema files are supposed to be local, part of the obsinfo distribution

        :returns: updated full uri
        :raises: ValueError
        """

        kwargs = self._ref_kwargs

        if kwargs['jsonschema']:
            return urlparse.urljoin(self.base_uri, self.__reference__["$ref"])
        else:
            dp = kwargs["datapath"]
            if not dp:
                msg = f'Error in datapath in full_uri, reference: {self.__reference__["$ref"]}'
                logger.error(msg)
                raise ValueError(msg)
            base_uri = Path(dp.build_datapath(self.__reference__["$ref"]))

            tupl = urlparse.urlsplit(str(base_uri))
            path = unquote(tupl.path)
            frag = tupl.fragment
            new_uri = Datapath.add_frag(path, frag)
            # define the uri depending on whether it is remote or not
            self.base_uri = new_uri if gitLabFile.isRemote(str(base_uri)) \
                else unquote(base_uri.as_uri())

            return(self.base_uri)
```

I don't like that (the file `discoveryfiles.py` only contains the
class `Datapath`, should rename to `datapath.py`)