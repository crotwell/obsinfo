.. _base_configuration_modifications_abstract:

*******************************
Abstract examples
*******************************

The following examples don't use
real _obsinfo_ structures, but their simplicity may help you to understand
the basic concepts.

Priority levels
-----------------------------------------------

.. code-block:: yaml

    base: 
        param1: 'A'
        param2: 'A'
        param3: 'A'
        param4: 'A'
        param5: 'A'
        configuration_default: 'CONFIGA'
        configuration_definitions:
            'CONFIGA':
                configuration-description: "The standard configuration"
            'CONFIGB':
                configuration-description: "The B definition"
                param1: 'B'
                param2: 'B'
    configuration: "CONFIGB"
    modifications:
        param2: 'C'
        param3: 'C'

will return :

.. code-block:: yaml

    param1: 'B'
    param2: 'C'
    param3: 'C'
    param4: 'A'
    param5: 'A'
    
because the configuration ``CONFIGB`` overrides the base values of ``param1``
and ``param2``, then the modifications ``param2`` and ``param3`` override the
result.


Only specified sub-elements are changed
-----------------------------------------------

.. code-block:: yaml

    base: 
        param1: 
            sub-param1: 'A'
            sub-param2: 'A'
    modifications:
        param1: {sub_param1: 'C'}

will return:

.. code-block:: yaml

    param1: 
        sub-param1: 'C'
        sub-param2: 'A'

and so on for deeper levels.

Multi-level priorities
------------------------------------------
Modifications > configuration > base priority
-----------------------------------------------

.. code-block:: yaml

    base: 
        paramA1:
            base: 
                paramB1: 'A'
                paramB2: 'A'
                configuration_default: 'CONFIGX'
                configuration_definitions:
                    'CONFIGX':
                        configuration-description: "The standard configuration"
                    'CONFIGY':
                        configuration-description: "The Y definition"
                        paramB1: 'D'
                        paramB2: 'D'
                    'CONFIGZ':
                        configuration-description: "The Z definition"
                        paramB1: 'E'
                        paramB2: 'E'
            configuration: "CONFIGY"
            modifications:
                paramB2: 'C'
        paramA2: 'A'
        configuration_default: 'CONFIGA'
        configuration_definitions:
            'CONFIGA':
                configuration-description: "The standard configuration"
            'CONFIGB':
                configuration-description: "The B definition"
                paramA1:
                    paramB1: 'B'
                paramA2: 'B'
    configuration: "CONFIGB"
    modifications:
        paramA1:
            configuration: 'CONFIGZ'
            paramB2: 'F'
        paramA2: 'C'

will return :

.. code-block:: yaml

    paramA1:
        paramB1: 'E'
        paramB2: 'F'
    paramA2: 'C'
    
because the top-level configuration of paramA1 ("CONFIGZ") overrides the lower level
configuration ('CONFIGY')a nd in turn overriden by the modification of paramB2 at the
same level.  This looks  complicated, but it is much clearer in a typical
information file, since each base definition is in a seperate file.:

.. code-block:: yaml

    base:  {$ref: "higher_level_base.yaml#higher_level_base"}
    configuration: "CONFIGB"
    modifications:
        paramA1:
            configuration: 'CONFIGZ'
            paramB2: 'F'
        paramA2: 'C'
    
and so we immediately see what is changed from the values in the
base specification files

stage_modifications > channel_modifications > modifications 
-----------------------------------------------

As complicated as it gets
------------------------------------------

Here should be multi-level example with channel-modifications and response-modifications.

Point out that this is much more complicated than anyone will see because
the lower levels are in separate files.
