.. _base_configuration_modifications_concrete:

*******************************
Concrete examples
*******************************

Specifying a datalogger's sampling rate
--------------------------------------------------

base configuration in the datalogger_base file

.. code-block:: yaml

    datalogger_base:
        equipment:
            model: "CS5321/22"
            type: "delta-sigma A/D converter + digital filter"
            description: "CS5321/22 A/D converter + digital filter"
            manufacturer: "Cirrus Logic"
            vendor: "various"
        configuration_default: "125sps"
        stages:
            - {base: {$ref: "dataloggers/stages/CS5321_FIR1.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
            - {base: {$ref: "dataloggers/stages/CS5322_FIR3.stage_base.yaml#stage_base"}}
        sample_rate: 125
        correction: 0.232
        configurations:
            "125sps":
                configuration_description: "125 sps"
            "250sps":
                configuration_description: "250 sps"
                sample_rate: 250
                correction: 0.116
                stages:
                    - {base: {$ref: "dataloggers/stages/CS5321_FIR1.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR3.stage_base.yaml#stage_base"}}
            "500sps":
                configuration_description: "500 sps"
                sample_rate: 500
                correction: 0.058
                stages:
                    - {base: {$ref: "dataloggers/stages/CS5321_FIR1.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR2.stage_base.yaml#stage_base"}}
                    - {base: {$ref: "dataloggers/stages/CS5322_FIR3.stage_base.yaml#stage_base"}}

Instantiation in the ``subnetwork`` file, (drilling through the ``instrumentation``
to the ``datalogger``):

.. code-block:: yaml

    ...
    stations:
        <STATION1>:
            ...
            instrumentation:
                base: {$ref: "my_instrumentation.instrumentation.yaml"}
                datalogger_configuration: "500sps"

Note 1:
    There is almost nothing specified for the "125sps" configuration because it
    is the default. For schema testing purposes, the base_level description must
    include all required parameters, so there's no point repeating them in the
    configuration definition.

Note 2:
    The text in the configuration description will be added to the
    ``equipment:description`` field, which will become (for the default
    configuration): "CS5321/22 A/D converter + digital filter [config: 125 sps]"

Note 3:
    If no "configuration_description" is provided, the configuration name will
    be added to the ``equipment:description``
    

Locations
--------------------------------------------------

base configuration in a ``location_base`` file

.. code-block:: yaml

    ---
    ...
    location_base:
        depth.m: 0
        geology: "unknown"
        vault: "Sea floor"
        uncertainties.m: {lon: 200, lat: 200, elev: 20}
        measurement_method: "Sea surface release point"
        configuration_default: "SEA_SURFACE"
        configurations:
            "SEA_SURFACE":
                configuration_description: "Standard sea-surface deployment"
            "MAYOBS_SEA_SURFACE":
                configuration_description: "Sea-surface deployment offshore Mayotte"
                measurement_method: "Sea surface release point"
                uncertainties.m: {lon: 300, lat: 300, elev: 20}
            "ACOUSTIC_SURVEY":
                uncertainties.m: {lon: 5, lat: 5, elev: 10}
                measurement_method: "Acoustic survey"
            "AIRGUN_SURVEY":
                uncertainties.m: {lon: 40, lat: 40, elev: 40}
                measurement_method: "Airgun survey"
                notes: ["Uncertainty will generally be least along-line"]
            "BUC_DIRECT":
                uncertainties.m: {lon: 5, lat: 5, elev: 10}
                measurement_method: "Short baseline transponder, seafloor release"
            "BUC_DROP":
                uncertainties.m: {lon: 20, lat: 20, elev: 20}
                measurement_method: "Short baseline transponder, near-seafloor release"`


Instantiation in the ``subnetwork`` file

.. code-block:: yaml

    ---
    ...
    subnetwork:
        ...
        stations:
            "LSVW":
                ...
                locations:
                    "00":
                        base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
                        configuration: "SURFACE_DROP"
                        position: {lon: -32.32504, lat: 37.29744, elev: -2030}
            "LSVE":
                ...
                locations:
                    "00":
                        base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
                        configuration: "ACOUSTIC_SURVEY"
                        position: {lon: -32.02504, lat: 37.25744, elev: -2130}
                        
Clock drift
--------------------------------------------------

base configuration in a timing_base file named ``Seascan_GNSS.timing_base.yaml``

.. code-block:: yaml

    ---
    ...
    timing_base:
        instrument: "Seascan MCXO, ~1e-8 nominal drift"
        reference: "GNSS"
        start_sync_instrument: 0


Instantiation in a ``subnetwork`` file

.. code-block:: yaml

    ---
    ...
    subnetwork:
        ...
        stations:
            "LSVW":
                ...
                processing:
                    - clock_correction_linear:
                        base: {$ref: "timing_bases/Seascan_GNSS.timing_base.yaml#timing_base"}
                        start_sync_reference: "2015-04-21T21:06:00Z"
                        end_sync_reference: "2016-05-28T20:59:00.32Z"
                        end_sync_instrument: "2016-05-28T20:59:03Z"
            "LSVE":
                ...
                processing:
                    - clock_correction_linear:
                        base: {$ref: "timing_bases/Seascan_GNSS.timing_base.yaml#timing_base"}
                        start_sync_reference: "2015-04-21T22:00:00Z"
                        end_sync_reference: "2016-05-29T20:59:59.32Z"
                        end_sync_instrument: "2016-05-30T21:00:00Z"
                        

Changing a sensor
--------------------------------------------------

using ``channel_modifications`` to change the type of hydrophone on the '*H' channel:

.. code-block:: yaml

    ...
    stations:
        <STATION1>:
            ...
            instrumentation:
                base: {$ref: "my_instrumentation.instrumentation.yaml"}
                channel-modifications:
                    '*H':
                        sensor:
                            base: {$ref: "my_other_sensor.sensor_base.yaml"}
                            serial_number: "H424B"


Chained configurations
--------------------------------------------------

In this example, the preamplifier ``configurations`` 
affect the ``configuration`` of the stages beneath them

The preamplifier_base definition:

.. code-block:: yaml

    ...
    preamplifier_base:
        equipment:
            model: "HYDRO-GAIN"
            type: "Analog gain/filter card"
            description: "SIO gain/filter card, hydro channel"
            manufacturer: "SIO or IPGP"
            vendor: ""
        configuration_default: "64x gain"
        stages:
            - {base: {$ref: "preamplifiers/stages/Scripps_SPOBS_HydroL22_theoretical.stage_base.yaml#stage_base"}}
        configurations:
            "16x gain":
                stages: [{configuration: "16x"}]
            "32x gain":
                stages: [{configuration: "32x"}]
            "64x gain":
                stages: [{configuration: "64x"}]
            "128x gain":
                stages: [{configuration: "128x"}]

the stage_base definition ``Scripps_SPOBS_HydroL22_theoretical.stage.yaml``

.. code-block:: yaml

    ---
    ...
    stage_base:
        description : "PREAMPLIFIER: SPOBS hydrophone or L22"
        input_units : {name: "V", description: "VOLTS"}
        output_units : {name: "V", description: "VOLTS"}
        gain: {value: 64, frequency: 10}
        filter :
            type : "PolesZeros"
            transfer_function_type: "LAPLACE (RADIANS/SECOND)"
            zeros : ['0 + 0j']
            poles : ['-6.667 + 0j']
        delay: 0
        configuration_default: "64x"
        configurations:
            "64x":
                gain: {value: 64, frequency: 10}
            "32x":
                gain: {value: 32, frequency: 10}
            "16x":
                gain: {value: 16, frequency: 10}
            "128x":
                gain: {value: 128, frequency: 10}
    


Instantiation in the ``subnetwork file`` configuring the preamplifier 
to "64x gain":

.. code-block:: yaml

    ---
    ...
    stations:
        <STATION1>:
            ...
            instrumentation:
                base: {$ref: "my_instrumentation.instrumentation.yaml"}
                datalogger_configuration: "500sps"
                channel_modifications:
                    '*-*': {datalogger: {serial_number: "0145"}}
                    'H-*': {preamplifier: {configuration: "64x gain"}}

Setting a custom value from the ``subnetwork`` file
--------------------------------------------------

You could  force the gain of one stage to a non-configured value
(1000 in this example), as follows:

.. code-block:: yaml

    ---
    ...
    stations:
        <STATION1>:
            ...
            instrumentation:
                base: {$ref: "my_instrumentation.instrumentation.yaml"}
                datalogger_configuration: "500sps"
                channel_modifications:
                    '*-*': {datalogger: {serial_number: "0145"}}
                    '*H':
                        preamplifier:
                            stage_modifications:
                                "0": {gain: {value: 1000}}}


A complete network file with channel_modifications:
--------------------------------------------------

.. code-block:: yaml

     ---
     format_version: "0.111"
     revision:
        authors:
            - {$ref: "authors/Wayne_Crawford.author.yaml#author"}
        date: "2019-12-19"
     subnetwork:
        network:
            $ref: "network/EMSO-MOMAR.network.yaml#network"
        operators:
            -
                agency: "INSU-IPGP OBS Park"
                contacts:
                    -
                        names: ["Wayne Crawford"]
                        emails:  ["crawford@ipgp.fr"]
                website:  "http://ipgp.fr"
        reference_names:
            operator: "INSU-IPGP"
            campaign: "MYCAMPAIGN"
        stations:
            "BB_1":
                site: "My favorite site"
                start_date: "2015-04-23T10:00:00"
                end_date: "2016-05-28T15:37:00"
                location_code: "00"
                instrumentation:
                    base: {$ref: "instrumentation/BBOBS1_2012+.instrumentation_base.yaml#instrumentation_base"}
                    channel_modifications:
                        "*-*": {datalogger: {configuration: "62.5sps"}}
                locations:
                    "00":
                        base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
                        configuration: "BUC_DROP"
                        position: {lon: -32.234, lat: 37.2806, elev: -1950}
                processing:
                    - clock_correct_linear_drift:
                        base: {$ref: 'timing_bases/SEASCAN_GNSS.timing_base.yaml#timing_base'}
                        start_sync_reference: "2015-04-23T11:20:00"
                        end_sync_reference: "2016-05-27T14:00:00.2450"
                        end_sync_instrument: "22016-05-27T14:00:00"
            "BB_2":
                notes: ["An example of changing the sensor"]
                site: "My other favorite site"
                start_date: "2015-04-23T10:00:00Z"
                end_date: "2016-05-28T15:37:00Z"
                location_code: "00"
                instrumentation:
                    base: {$ref: "instrumentation/BBOBS1_2012+.instrumentation_base.yaml#instrumentation_base"}
                    channel_modifications:
                       "*-*":
                        sensor: {configuration: "Sphere06"}
                        datalogger: {configuration: "62.5sps"}
                       "H-*": 
                          sensor: 
                            serial_number: "IP007"
                            stage_modifications:
                              "3": {gain: {value: 15}}
                locations:
                    "00":
                        base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
                        configuration: "ACOUSTIC_SURVEY"
                        position: {lon: -32.29756, lat: 37.26049, elev: -1887}
                processing:
                    - clock_correct_linear_drift:
                        base: {$ref: 'timing_bases/SEASCAN_GNSS.timing_base.yaml#timing_base'}
                        start_sync_reference: "2015-04-22T12:24:00"
                        end_sync_reference: "2016-05-28T15:35:00.3660"
                        end_sync_instrument: "2016-05-28T15:35:02" 
                        modifications:
                            start_sync_instrument: "2015-04-22T12:24:01"
                        notes: "The instrument clock was incorrectly synced at the start"
                        
  
