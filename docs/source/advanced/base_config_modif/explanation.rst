.. _base_configurations:

********************************
Base-configuration-modifications
********************************

Overview
************************************************

The full power of **obsinfo** is achieved using the
**base-configuration-modifications** protocol.
OBS are subject to many variations in configuration, including changes of
components in the field.
The aim of *obsinfo* is to have a relatively stable database of information
files for instrumentation, but also to account for reassembling the
in the field instrumentation, substituting stages and even whole components.
This must be reflected in the metadata without compromising the stability
of the instrumentation database.
The way to do this is through the **base-configuration-modifications**
nomenclature, which can change *any*
attribute in the configuration, down to serial numbers or pole/zeros of filters
at the channel level.

The ``configuration`` element allows pre-configured customization of
instrumentation.
It is also used to reduce repetition when specifying station locations
and clock drifts.   

instrument_components:
    ``datalogger``, ``sensor`` and ``preamplifier`` elements all have the same
    structure and are specified at the same level.  In the following,
    we refer to them collectively as ``instrument_components``

Structure
===================================================

Basic
---------------------------------------------------

All base-configuration-modifications elements can have these elements:

.. code-block:: yaml

    base: <file reference>
    configuration: <string>
    modifications: <element>

Only ``base`` is required    

Max structure
---------------------------------------------------

Here are all the possible elements (you'll never get them all together):

.. code-block:: yaml

    base: <file reference>
    configuration: <string>
    modifications: <element>
    channel-modifications: <element>
    response-modifications: <element>
    <shortcut1>:
    ...
    <shortcutN>:
    <non-base1>:
    ...
    <non-baseN>:
    
The order of priority is:
``non-base`` > ``stage_modifications`` > ``channel-modifications`` > ``shortcuts`` > 
``modifications`` > ``configuration`` > ``base``



only ``base`` and the ``<non-base>`` elements (if they exist) are required

Each element explained
===================================================

``base``
---------------------------------------------------

The ``base`` element defines the default parameters and optional
configurations. It is generally located in another file
and has the following structure:

.. code-block:: yaml

    <base-element1>: <element>
    <base-element2>: <element>
    ...
    configuration_default: <string>
    configurations:
        "<CONFIG1>":
            <base-elementX>:
            <base-elementY>:
            ...
            configuration_description: <string>
        "<CONFIG2>":
            ...
        ...
        
The values under the chose configuration will update the same elements in
the base definition.  This is a safe update: if, for example, a dictionary is
entered, but not all of its elements, only the specified elements will be changed.
Similarly, if an array is specified, only the corresponding indices will be changed,
and only the specified elements within these corresponding indices.  This gives
a lot of flexibility: for example, if you have a 5-element ``stages`` array and you
specify a 1-element ``stages`` array in the specified configuration, only the
first element will be changed and only those specified subelements will be changed
(this is useful for changing gains). Also, if you specify a 7-element ``stages`` array
then two stages will be added, which is useful for dataloggers which have a different
number of stages depending on the sampling rate.  But, if you are intending to
completely wipe out the existing stage information you have to provide a complete stage description,
as anything left over will be retained.  Also, there is no way to reduce the number
of stages, so your default configuration should have the least number of stages possible.

*It would be nice to allow the user to wipe out an existing set of stages,
but I don't know how to and keep the possibility of just changing a few values.
Also, as there are no ``stage_modifications`` in ``configurations`` (and it
would be complicated to add them) I don't know how else to change a single
value like "gain" in a single stage*

``configuration``
---------------------------------------------------

Allows you to chose one of the configurations specified in the ``base`` element

"[config: ``configuration_description``]" is appended to the end of:

- ``equipment:description`` (for ``instrumentation``, ``sensor``,
  ``preamplifier`` and ``datalogger``)
- ``stage:name``

``modifications``
---------------------------------------------------

The elements in ``modifications`` override elements
specified in ``base`` . Under ``modifications`` the user can specify a complete hierarchy down to the
lowest level.
In general, only the value(s) specified will be modified.
So if a "leaf" value is changed, such as gain value, only the gain value
for a particular stage will be changed.
If you want to replace the entire element, precede the key the character "^".
This is useful if you want to swap out a sensor, for example, without risking
retaining some of the original sensor's values/configurations.
This only works with ``channel`` sub-elements: ``^datalogger``, ``^sensor``,
``^orientation``, ``^preamplifier``, ``^identifiers``, ``^external_references``,
``^comments``, ``^extras``


``<non-base>``
---------------------------------------------------

``<non-base>`` elements are only specified at the top level, not in the ``base``.
These are values that we will ALWAYS want to specify in the subnetwork file.
They are: 
    - ``location:position``
    - ``clock_correction_linear:start_sync_reference``
    - ``clock_correction_linear:end_sync_reference``
    - ``clock_correction_linear:end_sync_instrument``


``<shortcut>``
---------------------------------------------------

Shortcuts to commonly-changed parameters.  Existing shortcuts are:

=============================================  ===========================================================   
  Shortcut                                           Standard  
=============================================  ===========================================================   
 ``instrumentation:serial_number``              ``instrumentation:modifications:equipment:serial_number``
---------------------------------------------  -----------------------------------------------------------
 ``instrumentation:datalogger_configuration``  ``instrumentation:modifications:datalogger:configuration``
---------------------------------------------  -----------------------------------------------------------
 ``datalogger:serial_number``                  ``datalogger:modifications:equipment:serial_number``
---------------------------------------------  -----------------------------------------------------------
 ``sensor:serial_number``                      ``sensor:modifications:equipment:serial_number``
---------------------------------------------  -----------------------------------------------------------
 ``preamplifier:serial_number``                ``preamplifier:modifications:equipment:serial_number``
=============================================  ===========================================================       

``channel-modifications``
---------------------------------------------------

Limit modifications to selected channels.
See `channel_modifications`_ for details.

``response-modifications``
---------------------------------------------------

Limit modifications to selected response stages.
See `stage_modifications`_ for details.


elements using base-configuration-modifications
************************************************

Here is a list of each element using the **base-configuration-modifications**
structure, each with its required or particular top-level elements (the
always-available but optional ``configuration`` and ``modification``
elements are not shown):

- ``instrumentation``

    .. code-block:: yaml
    
        instrumentation:
            base: <instrumentation_base file>
            serial_number: <str>             # shortcut
            datalogger_configuration: <str>  # shortcut
            channel-modifications: <element>

- instrument_component (``datalogger``, ``sensor``, or ``preamplifier``)

    .. code-block:: yaml
    
        instrument_component:
            base: <instrument_component_base file>
            serial_number: <str>            # shortcut
            response-modifications: <element>

- ``stage``

    .. code-block:: yaml
    
        stage:
            base: <stage_base file>

- ``locations``

    .. code-block:: yaml
    
        locations:
            <location_code>:
                base: <location_base file>
                position: {lat: <float>, lon: <float>, elev: <float>} # <non-base>
            <location_code>:
                ...

- ``clock_correction_linear```

    .. code-block:: yaml
    
        clock_correction_linear:
            base: <timing_base file>
            start_sync_reference: <ISOtime string>     # <non-base>
            end_sync_reference: <ISOtime string>       # <non-base>
            end_sync_instrument: <ISOtime string>      # <non-base>


Multi-level, multi-stage elements
************************************************

Level-based priority
===================================================

The ``instrumentation`` element has ``instrument_components`` as sub-elements,
which themselves have ``stages`` as sub-elements. Elements specified at a higher
level override those at a lower level
(``instrumentation`` > ``instrument_components`` > ``stage``).  A higher-level
``configuration`` will not override lower-level ``modifications``, so be
sparing in the use of modifications at the ``instrument_components`` and ``stage``
levels.  A higher-level ``base`` will cancel all lower-level ``configuration``
and ``modifications``, as those were set for another ``base``

.. _channel_modifications:

Channel_modifications
************************************************

The element ``channel_modifications`` is used to select a channel or
channels upon which to apply modificiations. 

Specifying the channel to change
===================================================

Channel identification is performed by an orientation-location code of the form:

.. code-block:: yaml

    "<orientation code>-<location code>"

For example, 

.. code-block:: yaml

    "Z-02"

The `orientation codes <Channel>`_ are defined as in the FDSN standard.
If the location code is omitted, a location code of "00" is assumed.
You can select all channels using "*" (although you could simply use
``modifications`` for this):

====================  =====================================================   
  Code                  Result  
====================  =====================================================    
 ``*`` or ``*-*``      All channels
--------------------  -----------------------------------------------------
    ``*-00``           All orientations with location "00"
--------------------  -----------------------------------------------------
    ``H-*``             Channel with orientation code H and all locations
--------------------  -----------------------------------------------------
    ``H``               Channel with orientation code H and location "00"
--------------------  -----------------------------------------------------
    ``1-01``             Channel with orientation code 1 and location "01"
====================  =====================================================

Orientation-location codes have priorities. The more specific code takes
precedence over the less specific code, and the orientation code specification
takes precedence over the location code specification.
For example, if ``*-*`` and ``1-01`` are specified, ``1-01`` takes precedence
for channels with orientation code ``1`` and location code ``01``.
And if ``*-00`` and ``H-*`` are specified, ``H-*`` takes precedence for
channels with orientation code ``H``.

Specifying an element to change
===================================================

The elements under a given **orientation-location code** can be:

- ``datalogger``
- ``sensor``
- ``preamplifier``
- ``orientation``

The first three allow you to change elements of one of these instrument components,
and/or the entire instrument component, while ``orientation``
lets you change the orientation.

The first three can be filled with any valid entities of their parent entity,
plus any of following keywords:

- ``base``: replaces the instrument component before applying the other
  entities.
- ``configuration``:  applies the configuration specified in the instrument
  component's ``configurations`` field
- ``modifications``, within which you can speficy a change to any subelement
- ``serial_number``,  a shortcut for ``{modifications: equipment: {serial_number: }}}``.
- ``stage_modifications``, which allow you to change parameters in individual
  stages
  
For example, if you want to specify the sensor's serial number you could
enter:

.. code-block:: yaml

    sensor:
        modifications:
            equipment: {serial_number: 'A1542'}

or

.. code-block:: yaml

    sensor:
        serial_number: 'A1542'

If you do both, the shortcut will overwrite the long version and you should
get a warning.

If you want to change the type of sensor, specify its serial number and
use a custom configuration, you could enter:

.. code-block:: yaml

    sensor:
        base: {$ref: 'sensors/T240.nanometrics.sensor.yaml#sensor'}
        configuration: "SINGLE-SIDED"
        serial_number: '235'


.. _stage_modifications:

stage_modifications
************************************************

Since stages have no name, they are referenced by their number, 
which specifies the order of the stage (starting at 1) within a given
instrument component (``sensor``, ``preamplifier``` or ``datalogger``).
Modifications to stages are specified using the keyword
``stage_modifications``.

If we want to change the gain the third stage of the sensor, the hierarchy
would look like this:

.. code-block:: yaml

 channel_modifications:
    "H-00":
         sensor:
             stage_modifications:
               "2": {gain: {value: 17}} 

If we wanted to replace all of the response stages, the file
would look like this:

.. code-block:: yaml

 channel_modifications:
    "H-00":
         datalogger:
             stages:
                - $ref: "responses/CS5321_FIR1.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR3.stage.yaml#stage"
                
Response modifications are very flexible. The label can be any of several
regular expressions.
Either a single number, as above, a list, a range or the wildcard "*".  Again,
you could just use ``modifications`` instead of ``stage_modifications:"*"``,
although the ``stage_modifications`` version is higher priority.

=========================== ========================================================   
  Stage Number                 Result  
=========================== ========================================================   
   ``*``                       All stages
--------------------------- --------------------------------------------------------
   ``[1,3]``                   Stages 1 and 3
--------------------------- --------------------------------------------------------
   ``[1-4]``                   Stages 1, 2, 3 and 4
=========================== ========================================================  


