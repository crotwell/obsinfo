********************************
Base-configuration-modifications
********************************

.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
  
  base_config_modif/base_config_explanation
  base_config_modif/base_config_abstract_examples
  base_config_modif/base_config_concrete_examples
