.. _channel_modifications:

****************
Channel modifications
****************

The full power of **obsinfo** is achieved using channel modifications.
OBS are subject to a lot of variations in configuration, even changes of
components in the field.
The aim of *obsinfo* is to have a relatively stable database of information
files for instrumentation.
But in actual operation it is common to reassemble the instrumentation,
substituting stages and even whole components.
This needs to be reflected in the metadata without compromising the stability
of the instrumentation database.
The way to do this is through channel modifications, which can change *any*
attribute in the configuration, down to serial numbers or pole/zeros of filters
at the channel level.

The attribute ``channel_modifications`` is used for this. Under this keyword
the user can specify a complete hierarchy down to the filter level. Only the
value(s) specified will be modified.
So if a "leaf" value is changed, such as gain value, only the gain value
for a particular stage will be changed.
But if a complete sensor is specified, the whole component along with its
stages and filters will be modified.

Specifying the channel to change
================================

A channel must be identified in order to apply particular modifications to it.
Channel identification is performed **not** by the channel label in the
``channels`` attribute, but by a channel code formed by the orientation and
the location in the form:

.. code-block:: yaml

 "<orientation code>-<location code>"

Recall `orientation codes <Channel>`_ are defined as an FDSN standard.

For example, 

.. code-block:: yaml

 "Z-02"

If the location code is omitted, a location code of "00" is assumed.
It is also possible to select all channels using "*":

==================== ==================================================   
  **Channel Code**     **Result**  
-------------------- --------------------------------------------------
    
 `"*"` or `"*-*"`      All channels
-------------------- --------------------------------------------------

    `"*-00"`           All orientations with location "00"
-------------------- --------------------------------------------------

    `"H-*"`             Channel with "orientation" H and all locations
-------------------- --------------------------------------------------

    `"H"`               Channel with "orientation" H and location "00"
-------------------- --------------------------------------------------


    `"1-01"`             Channel with orientation 1 and location "01"
==================== ==================================================

Channel codes have priorities. The more specific code will take precedence
over the less specific code, and the orientation code specification will take
precedence over the location code specification.
That is, if both `"*-*"` and `"1-01"` are specified, `"1-01"` will take
precedence.
And if `"*-00"` and `"H-*"` are specified, `"*-00"` will take precedence.

Specifying an element to change
================================

The elements under a given **Channel Code** can be:

- ``datalogger``
- ``sensor``
- ``preamplifier``
- ``orientation_code``

The first three allow you to change elements of one of these instrument components,
and/or the entire instrument component and ``orientation_code``
lets you change the orientation code.

The first three can be filled with any valid entities of their parent entity,
plus any of following keywords:

- ``base``, which replaces the instrument component before applying the other
  entities.
- ``configuration``, which applies the configuration specified in the instrument
  component's ``configuration_definitions`` field
- ``serial_number``, which sets the serial number for the given instrument
  component.  This is simply a shortcut for ``{equipment: {serial_number: }}``.
- ``response_modificaions``, which allow you to change parameters in individual
  stages
  
For example, if you want to specify the sensor's serial number you could
enter:

.. code-block:: yaml

    sensor:
        equipment: {serial_number: 'A1542'}

or

.. code-block:: yaml

    sensor:
        serial_number: 'A1542'

If you want to change the type of sensor, specify its serial number and
use a custom configuration, you could enter:

.. code-block:: yaml

    sensor:
        base: {$ref: 'sensors/T240.nanometrics.sensor.yaml#sensor'}
        serial_number: '235'
        configuration: "SINGLE-SIDED"


Specifying a stage to change
================================

Since stages have no name, they are referenced by their number, 
which specifies the order of the stage (starting at 0) within a given
instrument component (sensor, preamplifier or datalogger).
Modifications to stages are specified using the keyword
``stage_modifications``, as the keyword ``stages``
will overwrite all stages.

So, if we want to change the gain the third stage of the sensor, the hierarchy
would look like this:

.. code-block:: yaml

 channel_modifications:
     sensor:
         stage_modifications:
           "2": {gain: {value: 17}} 

If we, however, want to replace all of the response stages, the file
would look like this:

.. code-block:: yaml

 channel_modifications:
     datalogger:
         stages:
            - $ref: "responses/CS5321_FIR1.stage.yaml#stage"
            - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
            - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
            - $ref: "responses/CS5322_FIR3.stage.yaml#stage"
                
Response modifications are very flexible. THe label can be any of several
regular expressions.
Either the single number, as above, a list, a range or a wildcard "*",
which means all:

=========================== ========================================================   
  **Stage Number**             **Result**  
--------------------------- --------------------------------------------------------
   `"*"`                       All stages
--------------------------- --------------------------------------------------------
   "[1,2,3]"                   Second, third and fourth stages (stages start at 0)
--------------------------- --------------------------------------------------------
   "[1-3]"                     Same as above
=========================== ========================================================  

Example
================================
Here is a complete example of a network file with channel_modifications:

.. code-block:: yaml

 ---
 format_version: "0.110"
 yaml_anchors:
    obs_clock_correction_linear_defaults: &LINEAR_CLOCK_DEFAULTS
        time_base: "Seascan MCXO, ~1e-8 nominal drift"
        reference: "GPS"
        start_sync_instrument: 0
 revision:
    authors:
        - {$ref: "authors/Wayne_Crawford.author.yaml#author"}
    date: "2019-12-19"
subnetwork:
        network:
            $ref: "network/EMSO-AZORES.network.yaml#network"
        operators:
            -
                agency: "INSU-IPGP OBS Park"
                contacts:
                    -
                        names: ["Wayne Crawford"]
                        emails:  ["crawford@ipgp.fr"]
                website:  "http://ipgp.fr"
        reference_names:
            operator: "INSU-IPGP"
            campaign: "MYCAMPAIGN"
    stations:
        "BB_1":
            site: "My favorite site"
            start_date: "2015-04-23T10:00:00"
            end_date: "2016-05-28T15:37:00"
            location_code: "00"
            locations:
                "00":
                    base: {$ref: 'location_bases/BUC_DROP.location_base.yaml#location_base'}
                    position: {lon: -32.234, lat: 37.2806, elev: -1950}
            instrumentation:
                base: {$ref: "instrumentation/BBOBS1_2012+.instrumentation.yaml#instrumentation"}
                channel_modifications:
                    "*-*": {datalogger: {configuration: "62.5sps"}}
            processing:
                - clock_corrections:
                    linear_drift:
                        <<: *LINEAR_CLOCK_DEFAULTS
                        start_sync_reference: "2015-04-23T11:20:00"
                        start_sync_instrument: 0
                        end_sync_reference: "2016-05-27T14:00:00.2450"
                        end_sync_instrument: "22016-05-27T14:00:00"
        "BB_2":
            notes: ["An example of changing the sensor"]
            site: "My other favorite site"
            start_date: "2015-04-23T10:00:00Z"
            end_date: "2016-05-28T15:37:00Z"
            location_code: "00"
            locations:
                "00":
                    base: {$ref: 'location_bases/BUC_DROP.location_base.yaml#location_base'}
                    position: {lon: -32.29756, lat: 37.26049, elev: -1887}
            instrumentation:
                base: {$ref: "instrumentation/BBOBS1_2012+.instrumentation.yaml#instrumentation"}
                channel_modifications:
                   "*-*":
                    sensor: {configuration: "Sphere06"}
                    datalogger: {configuration: "62.5sps"}
                   "H-*": 
                      sensor: 
                        serial_number: "IP007"
                        stage_modifications:
                          "3": {gain: {value: 15}}
            processing:
                - clock_correct_linear_drift:
                        <<: *LINEAR_CLOCK_DEFAULTS
                        start_sync_reference: "2015-04-22T12:24:00"
                        start_sync_instrument: 0
                        end_sync_reference: "2016-05-28T15:35:00.3660"
                        end_sync_instrument: "2016-05-28T15:35:02" 
  

