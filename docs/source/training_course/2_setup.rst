.. _training_course_2:

**********************************************
Setting up
**********************************************

The following are basic steps to install and confirm that everything is working

Installation
=====================================================

Note: DOES NOT (YET) WORK IN WINDOWS: use a Mac or Linux computer

- Install obspy using their `Conda Installation instructions`_
- In your obspy environment, install obsinfo by typing ``pip install obsinfo``
- type ``pip list`` to confirm that your version is up-to-date (0.111.1.post5 at least)

More detailed instructions are in the :doc:`../installation`


Copy an example database into your own folder
=====================================================

- Create a working directory
- Go in there and run ``obsinfo-setup -d DATABASE``

A subfolder named ``DATABASE`` will be created.  Inside is a directory named
``Information_Files`` and under that are lots of subdirectories with obsinfo
information files.

Create a StationXML file
=====================================================

Copy one of the network files from the ``DATABASE/`` directory into your working
directory:

.. code-block:: bash

    cp DATABASE/Information_Files/subnetworks/EXAMPLE_essential.subnetwork.yaml .
    
Then run ``obsinfo-makeStationXML`` on it:

.. code-block:: bash

    obsinfo-makeStationXML EXAMPLE_essential.subnetwork.yaml .
    
A file named "EXAMPLE_essential.station.xml" should be created

Test the other command-line codes
=====================================================

Try the following two lines, to make sure that the other command-line codes work:

.. code-block:: bash

    obsinfo-validate EXAMPLE_essential.subnetwork.yaml
    obsinfo-print EXAMPLE_essential.subnetwork.yaml
    


.. _Conda Installation instructions: https://github.com/obspy/obspy/wiki/Installation-via-Anaconda