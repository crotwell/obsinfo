.. _Tutorial6:

********************************************
Building a datalogger information file
********************************************

Dataloggers are the components used to record the data treated by the
instrument stages before.
Their configuration files might get quite complex due to the number of necessary stages.

Dataloggers have the same common fields of any other instrument component,
with two extra fields: ``correction`` and ``sample_rate``,
which is the overall sample rate of the complete instrument.

**correction**

All stages can have nominal delays, but these delays sometimes need to be
corrected. The ``correction`` field accomplishes this. In StationXML
**correction** is an attribute of each stage.
However, as most delays come from the datalogger's digital filters, where they
are in samples which can only be converted to time once the stages sampling
rates are known, **obsinfo** requires you to specify the correction at the
``datalogger`` level. Two processes are allowed: 

#. Set the correction equal to the delay in every stage.  This is the most
   common case used by commercial dataloggers but it seems false if the datalogger
   doesn't REALLY correct time at every stage.
#. Set the correction equal to zero in all stages but the last, where the correction
   is set equal to a value provided by the user.  This corresponds to what most
   non-commercial dataloggers do.
   
    
The first case is activated by **NOT** specifying the ``correction`` in the
datalogger information file.
In other words, **obsinfo** assumes by default a "perfect" delay correction.
Note that this will also be applied to the non-datalogger stages, which should
probably be changed (either have a separate correction field for each
instrument_component, or specify ``correction`` at the channel level)

The second case is activated by specifying a ``correction`` in the datalogger
information file.  Note that, if your datalogger does not correct for the digital
delay, specifying ``correction: 0`` does the right thing, which is to set
``correction=0`` in each stage.`


Datalogger configuration definitions
---------------------------------------

The following paragraph requires the reader to have a minimal knowledge of signal treatment.

The code below is a real datalogger configuration file.
We see that this example has several response stages in each configuration,
based this time on the sample rate. This is due to the fact that each stage
with the FIR2 amd FIR3 filters has a decimation factor of 2: each one divides
the sample rate by two. FIR1 is actually an ADC, an analog to digital converter,
all previous stages in this instrument being analog, in particular the one
in the previous component, the preamplifier. FIR1 outputs a 32000 sps sample rate.
Thus, to get to a final 1000 sps sample rate we need four FIR2 and one FIR3,
each halving the sample rate. FIR2 and FIR3 have different coefficients and thus
both are necessary.
This means we need at least one FIR1, one FIR2 and one FIR3.
To keep halving the sample rate we simply add more FIR2.
So it's simple now to see now the difference in configurations: it's simply
adding an extra FIR2 each time.

.. code-block:: yaml

 ---
 format_version: "0.110"
 revision:
    date: "2019-12-20"
    authors:
        - $ref: 'authors/Wayne_Crawford.author.yaml#author'
 notes:
    - "Delay correction is hard-coded to 29 samples in LCHEAPO software"

 datalogger:
    equipment:
        model: "CS5321/22"
        type: "delta-sigma A/D converter + digital filter"
        description: "CS5321/22 delta-sigma A/D converter + FIR digital filter"
        manufacturer: "Cirrus Logic"
        vendor: "various"

    configuration_default: "125 sps"

    configuration_definitions:
        "62.5sps":
            sample_rate: 62.5
            correction: 0.464
            stages:
                - $ref: "responses/CS5321_FIR1.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR3.stage.yaml#stage"
        "125sps":
            sample_rate: 125
            correction: 0.232
            stages:
                - $ref: "responses/CS5321_FIR1.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR3.stage.yaml#stage"
        "250sps":
            sample_rate: 250
            correction: 0.116
            stages:
                - $ref: "responses/CS5321_FIR1.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR3.stage.yaml#stage"
        "500sps":
            sample_rate: 500
            correction: 0.058
            stages:
                - $ref: "responses/CS5321_FIR1.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR3.stage.yaml#stage"
        "1000sps":
            sample_rate: 1000
            correction: 0.029
            stages:
                - $ref: "responses/CS5321_FIR1.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR2.stage.yaml#stage"
                - $ref: "responses/CS5322_FIR3.stage.yaml#stage"

As can be seen, configuration definition labels are flexible and can suit any purpose imagined by the user. The best practice is to keep them short, explicit and consistent among different selectable configurations in the same group.

Next we will see stages and filters in more detail.

* :ref:`Next page, Building stage information files <Tutorial7>`
* :ref:`Previous page <Tutorial5>`
* :ref:`Back to start  <Tutorial1>`

