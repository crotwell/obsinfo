.. _Tutorial3:

***********************************************
A simple subnetwork file
***********************************************

Fundamentals of an information file
_____________________________________

Under the folder which will contain all your information files, create a folder called ``network``. 
Network information files can be part of or referenced by a campaign information file, but since 
we are not dealing with campaigns in *obsinfo* we will start with a network file.

Use your favourite text editor to create a network information file. Select as filename something 
meaningful, for example, single period OBS from INSU-IPGP would be SPOBS.INSU-IPGP. Then add the 
type of information file, ``network`` and the type of format, ``yaml``:

.. code-block:: yaml

  SPOBS.INSU-IPGP.subnetwork.yaml

The file should start with the three dashes; the next line must specify the
*obsinfo* version (required to know how to process the file).
There are several optional fields which we will omit in this first exercise.
It's a good idea, though, to include a revision with a date and an author.

.. code-block:: yaml

 ---
 format_version: "0.111"
 revision:
    authors:
        - names: ["Wayne Crawford"]
          agencies: ["IPGP", "CNRS"]
          emails: ["crawford@ipgp.fr"]
          phones: ["+33 01 83 95 76 63"]
    date: "2017-10-04"

Note the dash in a lonely line which indicates ``authors`` is a list.
Alternatively, since the author is probably an information that will be
repeated several times, this information can be stored in another file.
Let's say we create a folder named ``persons`` at the same level as ``network``
and then create a file inside that folder named ``Wayne_Crawford.person.yaml``.
We put the three fields previously under the ``authors`` key in that file,
so the file will look like:

.. code-block:: yaml

 ---
 format_version: "0.111"
 person:
     names: ["Wayne Crawford"]
     agencies: ["IPGP", "CNRS"]
     emails: ["crawford@ipgp.fr"]
     phones: ["+33 01 83 95 76 63"]

And then we reference the file with a ``$ref`` field in the original network file:

.. code-block:: yaml

 ---
 format_version: "0.110"
 revision:
    authors:
        - $ref: 'persons/Wayne_Crawford.person.yaml'
    date: "2017-10-04"

The effect of this, from the point of view of *obsinfo*, is to insert said part
of the ``Wayne_Crawford.person.yaml`` file instead of the ``$ref`` line.
If you do this *all* of the contents will be inserted.
This is undesirable as it will cause a syntax error; the system expects to
see the fields ``names``, ``emails``, ``agencies`` and ``phones``, not the three
dashes, the version, etc.
The solution is using an *anchor* or *fragment*, which will only insert the
contents of the field referenced by the anchor.
In this case, the anchor should be the field ``person``, and so the final syntax is the following:

.. code-block:: yaml


 ---
 format_version: "0.111"
 revision:
    authors:
        - $ref: 'persons/Wayne_Crawford.person.yaml#person'
    date: "2017-10-04"

File discovery
----------------

Finding the information files is one of the most important features in *obsinfo*.
Notice that the  pathnames in the examples above are not absolute (i.e. they
don't start at the base of the filesystem). 
In regular POSIX practice it is assumed that non-absolute paths are added to
the directory where the application is executed  (called the current working directory, or cwd).
To allow the user to store instrumentation files in one standard spot, *obsinfo*
will try to *discover* the file in one of several directories specified by the
variable ``obsinfo_datapath``, which is set by the ``obsinfo-setup`` application 
and found in the configuration file ``~/.obsinforc`` (``~`` is  your home directory). 
This works much in the same way that executables are found in Linux, MacOS
or Windows using the variable PATH.

Whenever a file in a ``$ref`` is specified without an absolute path, 
*obsinfo* will **sequentally** look for the file in all the directories specified
in ``obsinfo_datapath``.
A special keyword, ``GITLAB``, specifies a remote Gitlab repository. Here's an example:

``obsinfo_datapath``, as specified above, will always look in a local directory where the 
current examples are installed (via ``pip`` or ``conda``) and then, if not found, in the 
remote repository. This gives the user great flexibility, as (s)he can override an existing 
remote information file with a local one, change the order of discovery, etc.

In the end, you will create your own information files in a directory selected by you.
Then you  will have to edit the ``obsinfo_datapath`` variable to reflect the directory
from which you want to access your information files.

It is possible, although not recommended, to use absolute paths.

Use of slashes (/) instead of Windows backslashes (\) is recommended for
uniformity, so a file can be used on different operation systems.
However, if you use backslashes, *obsinfo* will understand them.


subnetwork
_____________________________________

The next field key, ``subnetwork``, starts the actual information.
You may specify several sub-elements which are listed in :ref:`subnetwork`,
but let's stick to the fundamentals:

.. code-block:: yaml

 subnetwork:
    network:
        code: "4G" # Either an FDSN provided network code or "XX" if no such code exists.
        name: "Short period OBSs" # Use the FDSN name if applicable
        start_date: "2007-07-01"
        end_date: "2025-12-31"
        description: "Short period OBS network example"
        operators: [{$ref: "operators/EMSO-AZORES.operator.yaml#operator"}]
    operators:
        -   {$ref: "operators/INSU-IPGP.operator.yaml#operator"}

The ``network`` section describes the network, of which the ``subnetwork`` is
a subset.

If the network has been declared to  FDSN, the information in ``network``
should correspond to the values on the FDSN site.
For information on how to request a network code or use a temporary code,
see this `link <http://docs.fdsn.org/projects/source-identifiers/en/v1.0/network-codes.html>`_ .


stations
_____________________________________

Stations belonging to the network are specified next.
They could, of coursem be put in a different file, but it is a best practice
to put the station information, so that one file contains all of the essential
deployment information.

The following attribute is, therefore, ``stations``.
One can specify as many stations as you want, but in this example we will only specify one.
Stations are identified by a one to five character code.
This code acts like a key, but be careful to surround it by quotes: otherwise
it will be flagged as an error by the JSON syntax validator.
The start_date and end_date should correspond to data start and end.
The site is described as a text field, and a location code is specified too.
More on locations later.

.. code-block:: yaml

   stations:
        "LSVW":
            site: "Lucky Strike Volcano West"
            start_date: "2015-04-22T12:00:00Z"
            end_date: "2016-05-28T21:01:00Z"
            location_code: "00"
            instrumentation:

                ...

instrumentation
_____________________________________

Stations must have an instrumentation, which specifies the entire data recording
system, from the sensor(s) to the datalogger.
The best practice is to specify these in a separate file, as the same instrumentation
(with different serial numbers and possibly different configurations) may be
used at several stations and/or for several experiments
.
The way to reference an instrumentation is the following:

.. code-block:: yaml

    instrumentation:
        base: {$ref: "instrumentation/SPOBS2.instrumentation.yaml#instrumentation"}
        datalogger_configuration: "250 sps"

``$ref`` is a different file in a folder called ``instrumentation``.
It is possible to specify several instrumentations in list format:

.. code-block:: yaml

    instrumentations:
            - $ref: "instrumentation/SPOBS2.instrumentation.yaml#instrumentation"
            - $ref: "instrumentation/BBOBS1_2012+.instrumentation.yaml#instrumentation"

Instrumentations can be configured in several ways.  One almost always provides
a ``datalogger_configuration``, to specify the sample rate and possibly digital
filters and/or gains.  Here is an example using all the possible keys:

.. code-block:: yaml

    instrumentation:
        base: {$ref: "instrumentation/SPOBS2.instrumentation.yaml#instrumentation"}
        serial_number: "01"
        configuration: "low power"
        modifications:
            ...
        channel_modifications:
            ...

The ``serial_number`` key lets you specify the instrumentation's serial number
The ``configuration`` key lets you select a configuration that has been pre-defined
for the instrumentation.
The ``modifications`` and ``channel_modifications`` keys let you modify individual
elements within the instrumentation: we will discuss them later.

Locations
_____________________________________

There must be at least one location in the file.
This is the position in geographical coordinates of the station, usually
referred as location "00".
Locations are specified as follows:

.. code-block:: yaml

    locations:
        "00":
            base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
            configuration: "ACOUSTIC_SURVEY"
            position: {lon: -32.32504, lat: 37.29744, elev: -2030}

There is a base, which is again a referenced file in a different folder (as best practice).
Again, there is a ``configuration``, which lets us chose which method was used to
locate the station (a default value is chosen if ``configuration`` is not provided).

Observe the difference between a list and a dictionary.
List items are separated by dashes, dictionaries need a key/value pair.
authors is a list.
locations is a dictionary.

However, there can be several locations.
That's the reason we have a ``location_code`` to specify the location corresponding to the station itself.
Other locations can be used if different channels have different positions or if
two channels have the same FDSN channel code, as will be seen shortly.

Channel modifications
_____________________________________

``channel_modifications`` allow you to apply modifications to certain channels in the 
instrumentation. 
For example, a change in an instrument_component configuration (more about this in 
the :ref:`Next page, Building a simple instrumentation file <Tutorial4>`).
Higher-level specifications take priority, so if channel_modifications are
specified both in the ``subnetwork`` file and in the ``instrumentation`` file,
the one in the subnetwork file takes precedence.

Channel modifications are what makes *obsinfo* so flexible.
We can specify several different configurations for the same components, and then select one of them.
We can also directly change other attributes in the instrumentation.
This allows components to be regarded almost as virtual descriptions which, when a particular configuration is selected, are instantiated into a specific, actual component.
Thus instrumentation files can be authentic libraries with little changes, while the changes of configuration are specified for each station in a network in a specific campaign.

Furthermore, these libraries can reside in a central GitLab repository, which is updated by 
authorized users and, being public, is available for reuse by all users. 
A user can even clone the repository in the regular GitLab way 
(see `here <https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html>`_) 
in order to work offline with the latest version of the repository.

Complete example
_____________________________________

*You can skip this section in a first reading.*

This is an actual subnetwork information file with the above information and
some optional fields which we have not yet described. Note the additions:

1) A second station
2) Use of yaml_anchors to avoid repeating information *in the same file*
3) Comments field in ``network``
4) a ``processing`` field in a ``station``. For more information on this, see :ref:`ProcessingClockCorrections`

.. code-block:: yaml

---
format_version: "0.111"
revision:
    authors:
        - {$ref: "persons/Wayne_Crawford.person.yaml#person"}
    date: "2019-12-19"
subnetwork:
    network:
        $ref: "networks/EMSO-AZORES.network.yaml#network"
    operators:
        -   {$ref: "operators/INSU-IPGP.operator.yaml#operator"}
    stations:
        "BB_1":
            site: "My favorite site"
            start_date: "2011-04-23T10:00:00"
            end_date: "2011-05-28T15:37:00"
            location_code: "00"
            locations:
                "00":
                    base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
                    configuration: "BUC_DROP"
                    position: {lon: -32.234, lat: 37.2806, elev: -1950}
            instrumentation:
                base: {$ref: "instrumentations/BBOBS1_pre2012.instrumentation_base.yaml#instrumentation_base"}
                configuration: "SN07"
                modifications:
                    datalogger: {configuration: "62.5sps"}
            processing:
                - clock_correction_linear:
                        base: {$ref: "timing_bases/Seascan_GNSS.timing_base.yaml#timing_base"}
                        start_sync_reference: "2015-04-23T11:20:00"
                        end_sync_reference: "2016-05-27T14:00:00.2450"
                        end_sync_instrument: "2016-05-27T14:00:00"       
        "BB_2":
            site: "My other favorite site"
            start_date: "2015-04-23T10:00:00Z"
            end_date: "2016-05-28T15:37:00Z"
            location_code: "00"
            notes: ["example of deploying with a different sphere"]
            instrumentation:
                base: {$ref: "instrumentations/BBOBS1_2012+.instrumentation_base.yaml#instrumentation_base"} 
                serial_number: "06"
                modifications:
                    datalogger: {configuration: "62.5sps", equipment: {serial_number: "26"}}
                    preamplifier: {equipment: {serial_number: "26"}}
                channel_modifications:
                    "1-*": {sensor: {configuration: "Sphere08"}}
                    "2-*": {sensor: {configuration: "Sphere08"}}
                    "Z-*": {sensor: {configuration: "Sphere08"}}
                    "H-*": {sensor: {configuration: "5004"}}
            locations:
                "00":
                    base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
                    configuration: "BUC_DROP"
                    position: {lon: -32.29756, lat: 37.26049, elev: -1887}
            processing:
                - clock_correction_linear:
                        base: {$ref: "timing_bases/Seascan_GNSS.timing_base.yaml#timing_base"}
                        start_sync_reference: "2015-04-22T12:24:00"
                        end_sync_reference: "2016-05-28T15:35:00.3660"
                        end_sync_instrument: "2016-05-28T15:35:02" 


In all *obsinfo* information files, you can add ``notes`` as a list of strings.
Notes are not put into the StationXML file, they only serve documentation
purposes within the information file.

``comments``, on the other hand, ``comments`` are added to StationXML files, and
can only be placed at levels which correspond to the levels in a StationXML file
with a ``Comment`` field.

``extras`` are key:value pairs for information that you wish to document/process
but do not correspond to existing obsinfo elements.
They are added as comments to the output StationXML file.

* :ref:`Next page, Building a simple instrumentation file <Tutorial4>`
* :ref:`Previous page <Tutorial2>`
* :ref:`Back to start  <Tutorial1>`
