.. _YAML: https://yaml.org/spec/1.2/spec.html
.. _JSON: https://www.json.org/json-en.html
.. _Dont Repeat Yourself: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself
.. _Yasmine: https://github.com/iris-edu/yasmine-stationxml-editor
.. _ObsPy: https://github.com/obspy/obspy/wiki

*******************************
Overview
*******************************

#################
Introduction
#################

**obsinfo** is a Python standalone application for creating FDSN-standard (StationXML) data and metadata for ocean bottom
seismometers using standardized, easy-to-read information files in `YAML`_ or `JSON`_ formats. The advantages
of **obsinfo** are the following:

* Easier input user format, as YAML and JSON syntax is simpler than XML
* Avoiding redundancy (`Don't Repeat Yourself`_)
* Flexibility to change configurations avoiding rewriting or modifying a new
  StationXML file each time 
* Autonomy of operation from Internet connections, being text-based
* Adapts StationXML to OBS-specific functionality
* Simple interface to other systems, such as `Yasmine`_

Unlike general systems like `Yasmine`_, **obsinfo** is designed to reflect
the vast variability of OBS equipment and the fact that changes on the field
are common and cannot be planned in advance.
Emergency procedures such as changing an equipment component or a stage in
the high seas need to accounted for.
The fact that the editing of information needs to occur without
connection to a central database is also a consideration. 
We have therefore chosen to avoid a GUI such as Yasmine and make **obsinfo**
totally text-based using regular files.
At the same time, every effort has been made to stay compatible with AROL,
which is based on an earlier version of **obsinfo** itself, so AROL yaml files
can be imported into **obsinfo**. 
However, as of this publication, `some differences exist <AROL11>`_.

Reuseability and lack of redundancy ais achieved by creating a repository of
instrumentations which is referenced time and again by different campaigns.
Flexibility is achieved by permitting these instrumentations to have several
selectable configurations, as well as the ability to make punctual changes to
*any* field via  channel modifications (see `Advanced Topics <AdvancedTopics>`_).

**obsinfo** also manages two "parallel" information file types: `experiment` and
`datacite`.  These files are not needed for creating StationXML but are useful
for validating data and metatdata and for creating DataCite files for DOI.
The datacite_ information file only contains fields that the lead scientist
should provide to a data center.

#################
Object Model
#################

.. image:: obsinfo_object_model.jpg

The reference manual is organized around the object model, in pages that describe each particular class. 
The object hierarchy starts in Class :ref:`Network`. You can navigate from one class to the other by using the **Relationships** section.


#################
Information Files
#################

The system is based on "`information files`_" in `JSON`_ or `YAML`_ format as
input, filled in by appropriate actors and broken down into different categories
to remove redundancy and simplify input as much as possible.
Information files are taken as input and converted first to an obsinfo object,
which in turn gets converted to an `ObsPy`_ object and then is output as a
StationXML file.


There are 6 main file types for StationXML and processing path creation:

+---------------------------+-----------------------+-----------------+---------------+
|    Name                   |    Description        |     Filled by   | When filled   |
+===========================+=======================+=================+===============+
| **subnetwork**            | Deployed stations,    |                 | after a       |
|                           | their instruments     | OBS facility    | campaign      |
|                           | and parameters        |                 |               |
+---------------------------+-----------------------+-----------------+---------------+
| **instrumentation**       | Instrument            | OBS facility    | new/changed   |
|                           | description           |                 | instruments   |
+---------------------------+-----------------------+-----------------+---------------+
| *instrument_components*   | Description of basic  | OBS facility    | when there    |
|         **sensors**       | components            | -or-            | are new       |                      
|                           |                       | component       | are new       |   
|         **components**    |                       | manufacturer    | components or |       
|                           |                       |                 | calibrations  |
|         **dataloggers**   |                       |                 |               |
+---------------------------+-----------------------+-----------------+---------------+
| **response_stage**        | Description of stages | OBS facility    | when there    |
|                           |                       | -or-            | are new       |
|                           | digitizers,           | component       | components or |
|                           | amplifiers/filters)   | manufacturer    | calibrations  |
+---------------------------+-----------------------+-----------------+---------------+
| **filter**                | Description of filters| OBS facility    | when there    |
|                           | amplifiers and        | -or-            | are new       |
|                           | digitizers used in    | component       | components or |
|                           | stages                | manufacturer    | calibrations  |
+---------------------------+-----------------------+-----------------+---------------+

Only the **subnetwork** files are OBS-specific and for most data-collection
campaigns they are all you'll need to fill out.

The **instrumentation** files and their subfiles could be replaced by existing
standards such as RESP files or the NRL (Nominal Response Library), but obsinfo provides 
a simpler and more standards-compliant way to specify the components, and 
it automatically calculates response sensitivities based on gains and filter
characteristics (using `ObsPy`_).  **obsinfo** instrumentation files could also
be used to make RESP-files and NRL directories (not yet coded). 

There are 2 other (optional) file types for quality control and Datacite creation:

+---------------------------+-----------------------+-----------------+---------------+
|    Name                   |    Description        |     Filled by   | When filled   |
+===========================+=======================+=================+===============+
| **experiment**            | Lists of stations     |                 |               |
|                           | facilities and        |                 |               |
|                           | participants, plus    | Chief scientist | after a data  |
|                           | desired verification. |                 | collection    |
|                           | NOT NEEDED FOR        |                 | campaign      |
|                           | PROCESSING            |                 |               |
+---------------------------+-----------------------+-----------------+---------------+
| **datacite**              | Scientist-specific    | Chief scientist | after a data  |
|                           | information for DOI   |                 | collection    |
|                           | datacite files        |                 | campaign      |
+---------------------------+-----------------------+-----------------+---------------+

File Hierarchy
====================

It is recommended to arrange the information files in a file hierarchy such as this:

.. code-block::

 /authors (contains files used in headers of other information files)
 /network 
 /instrumentation
 /sensors
 /preamplifiers
 /dataloggers
 [/instrumentation_componenent]/stages
 [instrumentation_componenent]/stages/filters

where [instrumentation_componenent] = sensors, preamplifiers or dataloggers.

**The hierarchy is completely up to the user**, including the names of the folders/directories. In fact, it is perfectly possible to put all the information in a single file, although it is not recommended as reusability of filters, stages or components depends on independent files.

To reference a file from within another file, use the *JREF* syntax:

::

    authors: - {$ref: "../authors/Wayne_Crawford.author.yaml#author"}

The effect of this directive is at the core of the philosophy of *obsinfo*, as it is this mechanism which allows reuse: it substitutes the content
of the key ``author`` within the file ``../authors/Wayne_Crawford.author.yaml`` as the value of the key ``authors``. If you want to include the complete file, remove the ``#author`` anchor.

File Naming Convention
========================

While there is flexibility about the folder hierarchy, information files **must** follow the following naming convention:

\<descriptive file name\>.\<obsinfo file type\>.\<file format\>

where 

 **\<descriptive file name\>** usually includes vendor and configuration shorthand to make the file easily identifiable by users,

 **\<obsinfo file type\>** is one of ``campaign``, ``network``, ``instrumentation``, ``sensor``, ``preamplifier``, ``datalogger``, ``stage``, ``filter``

 **\<file format\>** is one of ``yml``, ``yaml`` or ``json``.


Examples:

* ``TI_ADS1281_FIR1.stage.yml`` is a stage with a Texas Instruments FIR filter nested in it, in YAML format. 
* ``BBOBS.INSU-IPGP.network.json`` is a network of broad-band stations deployed by INSU IPGP, in JSON format.  

There are three resources to look up the exact syntax for each information file. One is the :ref:`Tutorial1` which takes you step by step building a
typical hierarchy of information files. The different :ref:`Classes` pages have a complete explanation of all the attributes in the class. Unless otherwise noted, attributes in the file have the same syntax as attributes in the class. The third resource is the formal syntax of the file, which is a JSON schema, which is always referenced in the Class page.

File Metadata
=================

All information files contain common metadata


* ``format_version:`` - This is a required field. It reflects the template version against which this file must be validated

* ``revision:`` - Revision information (date in particular) to keep change control of file. 

   - ``date:`` - date of revision  

   - ``authors:`` - authors of revision, usually a reference to an author file

* ``notes:`` - Optional extra information which will not be put in final metadata.
* ``yaml_anchors:`` - YAML anchors to avoid redundancy, for advanced YAML users. `Here <https://medium.com/@kinghuang/docker-compose-anchors-aliases-extensions-a1e4105d70bd>`_ is a guide on how to use YAML anchors.



#####################
Resources
#####################

`Installation and Startup Guide  <InstallStartup>`

`Tutorial <Tutorial1>` 

`Class Reference Pages <class11>`

`Information File Templates <FILE>`

For the YAML specification, see `YAML`_ . For a tutorial, see `YAML Tutorial <https://www.tutorialspoint.com/yaml/index.htm>`_

For the JSON specification, see `JSON`_ . For a tutorial, see `JSON Tutorial <https://www.tutorialspoint.com/json/index.htm>`_

For the JREF notation, see `JREF Notation <https://tools.ietf.org/id/draft-pbryan-zyp-json-ref-03.html>`_

For the StationXML reference, see `FDSN StationXML Reference <http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#>`_
