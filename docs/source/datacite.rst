.. _YAML: https://yaml.org/spec/1.2/spec.html
.. _JSON: https://www.json.org/json-en.html


*******************************
  Datacite information files
*******************************

#################
Introduction
#################

datacite information files are used to allow the principal scientist to provide
information for DOI datacite files.  The DataCite standard is described at
https://schema.datacite.org/meta/kernel-4.3/ and there are some tools for
directly creating Datacite files, such as
https://github.com/claudiodsf/datacite-metadata-generator

The datacite information files simplify entering
values for principal scientists, by proposing a limited but usually sufficient
list of fields and values for these fields, as shown below.

One of the simplifications is that only a few identifier schemes are proposed:
ORCID for people and ROR or GRID for organizations.  To find the ROR URI for
your organization, you can go to https://ror.org and search for your
organization, or just do a web search on "{your institution} ROR"

The fields provided are:

=====================  ===  ===================  ==================================================
    Name                #     Type               Description
=====================  ===  ===================  ==================================================
`title`                1    string               Title for the experiment
`description`          1    string               Description of the experiment
`creators`             1    list of Entity_      List of authors (Entity objets), in publication order
`subjects`             1    list of string       Keywords                      |
`contributors`         1    object
`  data_collectors`    1    list of Entity_      Data collectors not in `creators` (engineers, scientists)   
`  project_leader`     0-1  Entity_               use only if not the same as the first creator     
`  project_members`    0-1  list of Entity_      Participants not in `creators` or `data_collectors`
`related_identifiers`  0-1  list of RelIdent_    Things like articles, other DOIs, etc
`place`                1    string               The experiment location
`funders`              0-1  list of Funder_      Experiment funders
=====================  ===  ===================  ==================================================


Entity
-------------------------

describes people or organizations in the lists of creators or contributors:

===============  ===  ======  ==================================================
    Name           #  Type       Description
===============  ===  ======  ==================================================
`name`           1    string  "family, given" if Personal name       
`type`           0-1  string  'Organization', if not a Personal name 
`identifier`     0-1  string  affiliation unique identifier (i.e. 'https://orcid.org/0000-0002-3260-1826') |
`scheme`           1  string  identifier scheme, required if `identifier` provided (i.e. 'ORCID')
`affiliations`   0-1  list    list of affiliations                          
`   name`         1   string  affiliation name (i.e 'IPGP')                 
`   identifier`  0-1  string  affiliation unique identifier (i.e. 'https:/ror.org/004gzgz66')
`   scheme`        1  string  identifier scheme, required if `identifier` provided (i.e. 'ROR')
===============  ===  ======  ==================================================

RelIdent
-------------------------

describes related identifiers, such as DOIs and articles

============  ===  ===========  ==================================================
    Name        #  Type         Description
============  ===  ===========  ==================================================
`identifier`  1    string       affiliation unique identifier (i.e. 'https://orcid.org/0000-0002-3260-1826') |
`scheme`      1    sc_string_   identifier scheme
`relation`    1    rel_string_  relation between the identifier and the current datacite:                         
============  ===  ===========  ==================================================

An `sc_string` is a string chosen from: ARK arXiv bibcode DOI EAN13 EISSN Handle
IGSN ISBN ISSN ISTC LISSN LSID PMID PURL UPC URL URN w3id


A `rel_string` is a string chosen from: IsCitedBy Cites IsSupplementTo
IsSupplementedBy IsContinuedBy Continues IsDescribedBy Describes HasMetadata
IsMetadataFor HasVersion IsVersionOf IsNewVersionOf IsPreviousVersionOf
IsPartOf HasPart IsPublishedIn IsReferencedBy References IsDocumentedBy
Documents IsCompiledBy Compiles IsVariantFormOf IsOriginalFormOf IsIdenticalTo
IsReviewedBy Reviews IsDerivedFrom IsSourceOf IsRequiredBy Requires
IsObsoletedBy Obsoletes

Funder
-------------------------

describes funders

==================  ======  ========  ==================================================
    Name              #     Type         Description
==================  ======  ========  ==================================================
`name`              1      string    Funder's name
`identifier`        0-1    string    Funder's unique identifier (i.e. 'https:/ror.org/004gzgz66')
`scheme`              1    string    identifier scheme (i.e. 'ROR'), required if `identifier` provided
`award_number`      0-1    string                     
`award_URI`         0-1    string                     
`award_title`       0-1    string                     
==================  ======  ========  ==================================================


Below is an example datacite information file from
`_examples/datacite/EMSO-MOMAR_OBS.datacite.yaml`

.. code-block:: yaml

    ---
    format_version: "0.110"
    datacite:
        title : EMSO-MOMAR
        description : "Seismology component of a multi-year multidisciplinary
                      geophysical observatory on Lucky Strike volcano,
                      Mid-Atlantic Ridge (37°N, 32°W)"
        creators:
            -   name : Cannat, Mathilde
                identifier: https://orcid.org/0000-0002-5157-8473
                scheme: ORCID
            -   name : Crawford, Wayne
                identifier: https://orcid.org/0000-0002-3260-1826
                scheme: ORCID
            -   name : IPGP Marine Geosciences Team
                type : Organization
                affiliations : 
                    - name: IPGP
                      identifier: https://ror.org/004gzqz66
                      scheme : ROR
        subjects :
            - Mid-ocean ridge volcanos
            - Hydrothermal fields
        dates_collected:  2007-07-18/2022-08-24
        contributors:
            data_collectors:
                -   name: Daniel, Romuald
                    affiliations:
                        - name: INSU-IPGP OBS Facility
                -   name: Besancon, Simon
                    affiliations:
                        - name: INSU-IPGP OBS Facility
                -   name: INSU-IPGP OBS Facility
                    type: Organization
                    identifier: "Need to make an ROR for the facility"
                    scheme: ROR
            project_members:
                -   name: Bohidar, Soumya
                    affiliations:
                    -   name: IPGP
        related_identifiers:
            -   identifier: www.doi.org/1234567
                scheme: DOI
                relation: HasMetadata
        place: Lucky Strike volcano, Mid-Atlantic Ridge
        funders:
            -   name: ANR 
                identifier: https://ror.org/00rbzpz17
                scheme: ROR
                award_URI: https://anr.fr/Project-ANR-14-CE02-0008
                award_title: "Magma chamber to micro-habitats : dynamics of deep sea 
                             hydrothermal ecosystems – LuckyScales"