*****************
Examples
*****************

Examples of information files, from the most basic to the most
complete.

.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
  
  examples/1_basic_flat
  examples/2_basic_atomic
  examples/3_basic_configuration
