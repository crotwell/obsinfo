*******************
Basic - two channel
*******************

Here is an example of a valid information file containing only
required fields, all in one file.  To save space, this is a two-channel
instrument.  Generally, to save space and avoid repetition, information files
are split into parts connected by JSON references: in the next secton you will
find the  same information as below, but spit into "atomic" files.

The file below can be found in
obsinfo/_examples/Information_Files/subnetworks/EXAMPLE_essential_noRefs.subnetwork.yaml


.. code-block:: yaml

    ---
    format_version: "0.111"
    revision:
        authors:
            - names: ["Wayne Crawford"]
              agencies: ["IPGP", "CNRS"]
              emails: ["crawford@ipgp.fr"]
              phones:  ["+33 01 83 95 76 63"]
              date: "2019-12-19"
    subnetwork:
        operators:
            - agency: "INSU-IPGP OBS operator"
              contacts: 
                  -
                      names: ["Wayne Crawford"]
                      emails: ["crawford@ipgp.fr", "parc-obs@services.cnrs.fr"]
                      phones: ["+33 (0)6 51 51 10 54"]
                  -
                      names: ["Romuald Daniel"]
              website: "http://parc-obs.insu.cnrs.fr"
        network:
            code: "4G"
            name: "EMSO-AZORES"
            start_date: "2007-07-01"
            end_date: "2025-12-31"
            description: "Local seismological network around the summit of Lucky Strike volcano"
            comments: 
                - "Lucky Strike Volcano, North Mid-Atlantic Ridge"
        stations:
            "BB_1":
                site: "My favorite site"
                start_date: "2011-04-23T10:00:00"
                end_date: "2011-05-28T15:37:00"
                location_code: "00"
                locations:
                    "00":
                        base:
                            depth.m: 0
                            geology: "unknown"
                            vault: "Sea floor"
                            uncertainties.m: {lon: 20, lat: 20, elev: 20}
                            measurement_method: "Short baseline transponder, near-seafloor release"
                        position: {lon: -32.234, lat: 37.2806, elev: -1950}
                instrumentation:
                    base: "INSU_BBOBS"
                    configuration: "SN07"
                    modifications:
                        datalogger: {configuration: "62.5sps"}
                processing:
                    - clock_correction_linear:
                            base:
                                instrument: "Seascan MCXO, ~1e-8 nominal drift"
                                reference: "GNSS"
                                start_sync_instrument: 0
                            start_sync_reference: "2015-04-23T11:20:00"
                            end_sync_reference: "2016-05-27T14:00:00.2450"
                            end_sync_instrument: "2016-05-27T14:00:00"       
