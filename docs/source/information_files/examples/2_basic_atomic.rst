**************************
Basic atomic - two channel
**************************

Here is an example of a the same information as in basic_flat, divided across the
standard obsinfo file structure.
You can see that there are many fewer lines and less repetition.

The file below can be found in
obsinfo/_examples/Information_Files/subnetworks/EXAMPLE_essential.subnetwork.yaml,
, with configuration definitions removed  for simplicity. To see what configuration
definitions can do, see the basic_configuration document.
The referenced files are found below obsinfo/_examples/Information_Files/.  

.. code-block:: yaml

    ---
    format_version: "0.111"
    revision:
        authors:
            - {$ref: "persons/Wayne_Crawford.person.yaml#person"}
        date: "2019-12-19"
    subnetwork:
        operators:
            -   {$ref: "operators/INSU-IPGP.operator.yaml#operator"}
        network:
            $ref: "networks/EMSO-AZORES.network.yaml#network"
        stations:
            "BB_1":
                site: "My favorite site"
                start_date: "2011-04-23T10:00:00"
                end_date: "2011-05-28T15:37:00"
                location_code: "00"
                locations:
                    "00":
                        base: {$ref: 'location_bases/INSU-IPGP.location_base.yaml#location_base'}
                        configuration: "BUC_DROP"
                        position: {lon: -32.234, lat: 37.2806, elev: -1950}
                instrumentation:
                    base: {$ref: "instrumentations/BBOBS1_pre2012.instrumentation_base.yaml#instrumentation_base"}
                    configuration: "SN07"
                    modifications:
                        datalogger: {configuration: "62.5sps"}
                processing:
                    - clock_correction_linear:
                            base: {$ref: "timing_bases/Seascan_GNSS.timing_base.yaml#timing_base"}
                            start_sync_reference: "2015-04-23T11:20:00"
                            end_sync_reference: "2016-05-27T14:00:00.2450"
                            end_sync_instrument: "2016-05-27T14:00:00"
