*****************************************
base-configuration-modification
*****************************************

Several elements in information files are expressed using
"base-configuration-modification".
This allows easy specifiation or modification of values with minimum
repetition.

The base and configurations are defined in an element named ``<field>:base`` and
are implemented and modified in an element just above named ``<field>``, where
``field`` could be ``instrumentation``, ``location``, ``sensor``, ``datalogger``,
``preamplifier`` or ``stage``.
For example:

.. code-block:: yaml

    datalogger:
        base:
            equipment: ...
            stages: ...
            sample_rate: 125
            correction: 0.232
            configuration_default: "125sps"
            configurations:
                "62.5sps":
                    configuration_description: "62.5 sps"
                    sample_rate: 62.5
                    correction: 0.464
                    stages: ...
                "125sps":
                    configuration_description: "125 sps"
                "250sps":
                    configuration_description: "250 sps"
                    sample_rate: 250
                    correction: 0.116
                    stages: ...
        configuration: "250sps"
        modification:
            correction: 1.0
            equipment:
                serial_number: "F05"
        <shortcuts>
        <non-base_elements>
        <specific_modifications>
        
If the datalogger base were specified and no ``configuration``
was specified, then the "125sps" configuration would be used.
The ``configuration_description`` is appended to the equipment
``description``.

In this example the "250sps" ``configuration`` is specified, so the values
in the "250sps" configuration will replace the values with the same
names in the ``base`` definition.

The ``modifications`` field allows one to further modify the values.  The values
specified in ``modifications`` overwrite corresponding values in both the
``base`` and the ``configuration``.

If no ``configuration_default`` is specified and ``configurations`` is
specified, the level above MUST specify the configuration.  This will not be
caught by ``obsinfo-validate``: only by ``obsinfo_print`` or ``obsinfo_makeStationXML``.

The ``base`` element must include all required fields for the element (needed for
``obsinfo-validate``).
In our example, since this base configuration specifies the values
corresponding to a 125 sps sampling rate, the "125sps" configuration is nearly
empty.

``configurations`` are optional, so if you only have one configuration
you don't have to add additional elements.  Some elements (such as ``timing_bases``)
may nneed a configuration but the base-configuration-modification system
provides a consistent interface that allows us to easily add in information,
in this case sync times.

In almost all cases, the element definition should be in a separate
file: for example the datalogger definition file would contain:

.. code-block:: yaml

    datalogger_base:
        equipment: ...
        stages: ...
        sample_rate: 125
        correction: 0.232
        configuration_default: "125sps"
        configurations:
            "62.5sps":
                configuration_description: "62.5 sps"
                sample_rate: 62.5
                correction: 0.464
                stages: ...
            "125sps":
                configuration_description: "125 sps"
            "250sps":
                configuration_description: "250 sps"
                sample_rate: 250
                correction: 0.116
                stages: ...

and the call to it would look like:

.. code-block:: yaml

    datalogger:
        base: {$ref: 'dataloggers/LC2000.datalogger_base.yaml#datalogger_base'}
        configuration: "250sps"
        modifications:
            correction: 1.0
            equipment:
                serial_number: "F05"


        

---------------------------------
Shortcuts
---------------------------------

Shortcuts allow you to quickly enter common modifications.
For example, the ``datalogger`` element has a shortcut called ``serial_number``
that duplicates ``datalogger: {equipment: {serial_number:}}``, allowing you to
write the above code as:

.. code-block:: yaml

    datalogger:
        base: {$ref: 'dataloggers/LC2000.datalogger_base.yaml#datalogger_base'}
        configuration: "250sps"
        serial_number: "F05"
        modifications:
            correction: 1.0

Shortcuts override equivalent entries at the ``modifications`` level.
They are:

================================================   ================================================================================
Shortcut                                           Replaces
================================================   ================================================================================
``instrumentation: {datalogger_configuration:}``   ``instrumentation: {channel_modifications: {'*-*': {datalogger: {configuration:}}}}``
``instrumentation: {serial_number:}``              ``instrumentation: {modifications: {equipment: {serial_number:}}}``
``datalogger: {serial_number:}``                   ``datalogger: {modifications: {equipment: {serial_number:}}}``
``sensor: {serial_number:}``                       ``sensor: {modifications: {equipment: {serial_number:}}}``
``preamplifier: {serial_number:}``                 ``preamplifier: {modifications: {equipment: {serial_number:}}}``
================================================   ================================================================================

---------------------------------
Non-base elements
---------------------------------

Non-base elements are not specified in the ``base`` element, because they are expected
to be different for each deployment.  They are:

location:
------------------------

.. code-block:: yaml

    position: {lat: <number>, lon: <number>, elev: <number>}

clock_correction_linear:
-------------------------

.. code-block:: yaml

    start_sync_reference: <date-time>
    end_sync_reference: <date-time>
    end_sync_instrument: <date-time>

---------------------------------
Specific modifications
---------------------------------

Specific modifications apply only to certain channels or even certain stages
of a given channel.  They are specified using the ``channel_modifications``
and ``stage_modifications`` elements at the ``instrumentation``,
``datalogger``, ``sensor`` or ``preamplifier`` level.
Details are provided in :ref:`channel_modifications` and in
:ref:`stage_modifications`.