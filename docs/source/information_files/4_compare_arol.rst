****************************
Comparison with AROL/YASMINE
****************************

The goals and technologies of obsinfo are similar to those found in
YASMINE_ and the format of the Atomic Response Object Library
(AROL_) files used by YASMINE is the very similar to that of the Datalogger,
Sensor and Preamplifier files (and underlying ``filter`` files) used by obsinfo.
The major differences are:
- obsinfo is a completely file-based solution, designed for processing large
  sets of similar instruments.  Yasmine is a GUI (YASMINE_EDITOR_) or a
  command-line interface (YASMINE_CLI_) to modify existing StationXML files.
  obsinfo is thus best compared with a combination of YASMINE_EDITOR_ (to
  create a base StationXML file for a given configuration), followed by
  YASMINE_CLI_ (to  enter station-specific modifications to this configuration)
- obsinfo holds its configuration information inside the instrument component 
  (DataLogger, PreAmplifier, Sensor) files, whereas AROL has a file for each
  configuration and uses an accompanying configuration file to choose between
  them
- obsinfo adds processing and ocean-bottom seismology specific fields, to
  facilitate the notation and correction for clock problems such as drift
  and leap-seconds.
- obsinfo uses ``instrumentation``` files to represent specific OBSs
- obsinfo uses ``network`` files to control all this

The YASMINE Commmand-line interface (_YASMINE_CLI)

.. _YASMINE: https://github.com/iris-edu/yasmine-stationxml-editor
.. _YASMINE_EDITOR: https://github.com/iris-edu/yasmine-stationxml-editor
.. _YASMINE_CLI: https://github.com/iris-edu/yasmine-stationxml-cli
.. _AROL: https://gitlab.com/resif/arol

Nomenclature
=============================================
We use the term "InstrumentComponent" for Dataloggers, Preamplifiers and
Sensors.

AROL verus obsinfo configuration methods
=============================================

Let's compare the AROL implementation of a Nanometrics Taurus datalogger with
that of obsinfo.  My AROL example comes from gitlab/RESIF/AROL/sources/Dataloggers.

The AROL configuration file is called `nanometrics.yaml` and includes Centaur,
Taurus and .. dataloggers.  For brevity, I only include the Taurus loggers:

.. code-block:: yaml

    mandatory_filters: [digitizer_manufacturer, digitizer_model, sampling_rate]

    filters:
      - name: Digitizer manufacturer
        code: digitizer_manufacturer
        help: Select the manufacturer of your digitizer

      - name: Digitizer model
        code: digitizer_model
        help: Select the model of your digitizer

      - name: Version
        code: digitizer_version
        help: Select the version of digitizer you have

      - name: DC removal on/off
        code: dc_removal
        help: Is DC removal filter activated or not

      - name: Samples per seconds
        code: sampling_rate
        help: Select the sampling rate for this channel

      - name: Frontend gain
        code: preamplifier_gain
        help: Select the preamplifier gain (frontend)

    responses:
      - path: nanometrics/TAURUS-G-1.response.yaml
        applicable_filters:
          digitizer_manufacturer: Nanometrics
          digitizer_model:  Taurus
          preamplifier_gain: Gain 1 (1x - 0dB - 16Vpp)

      - path: nanometrics/TAURUS-G-04.response.yaml
        applicable_filters:
          digitizer_manufacturer: Nanometrics
          digitizer_model:  Taurus
          preamplifier_gain: Gain 0.4 (0.4x - 0dB - 40Vpp)

      - path: nanometrics/TAURUS-G-2.response.yaml
        applicable_filters:
          digitizer_manufacturer: Nanometrics
          digitizer_model:  Taurus
          preamplifier_gain: Gain 2 (1x - 0dB - 8Vpp)

      - path: nanometrics/TAURUS.10.response.yaml
        applicable_filters:
          digitizer_manufacturer: Nanometrics
          digitizer_model: Taurus
          sampling_rate: 10 sps

      - path: nanometrics/TAURUS.40.response.yaml
        applicable_filters:
          digitizer_manufacturer: Nanometrics
          digitizer_model: Taurus
          sampling_rate: 40 sps

      - path: nanometrics/TAURUS.50.response.yaml
        applicable_filters:
          digitizer_manufacturer: Nanometrics
          digitizer_model: Taurus
          sampling_rate: 50 sps

      - path: nanometrics/TAURUS.100.response.yaml
        applicable_filters:
          digitizer_manufacturer: Nanometrics
          digitizer_model: Taurus
          sampling_rate: 100 sps

      - path: nanometrics/TAURUS.200.response.yaml
        applicable_filters:
          digitizer_manufacturer: Nanometrics
          digitizer_model: Taurus
          sampling_rate: 200 sps

      - path: nanometrics/TAURUS.250.response.yaml
        applicable_filters:
          digitizer_manufacturer: Nanometrics
          digitizer_model: Taurus
          sampling_rate: 250 sps

and a few examples of the AROL response files are:

nanometrics/TAURUS-G-1.response.yaml
-------------------------------------
.. code-block:: yaml

    ---
    format_version: '0.106'
    response:
      decimation_info:
        correction: true
      stages:
        - input_units:
            name: V
            description: Volts
          output_units:
            name: V
            description: Volts
          gain:
            value: 1.0
            frequency: 0.0
          filter:
            type: ANALOG
          name: ' AMPLIFIER_FILTER'
          delay: 0.0
    extras:
      DBIRD_response_type: THEORETICAL
      Number_of_zeroes: '0'
      Number_of_poles: '0'
    notes:
      - '#   Diviseur par 1 en preampli'
      - 'Response_type                        : THEORETICAL   # optionel'
      - 'Input_unit                           : V             # optionel'
      - 'Output_unit                          : V             # optionel'
      - 'Transfer_normalization_frequency     : 0             # optionel'

nanometrics/TAURUS-G-2.response.yaml
-------------------------------------
.. code-block:: yaml

    ---
    format_version: '0.106'
    response:
      decimation_info:
        correction: true
      stages:
        - input_units:
            name: V
            description: Volts
          output_units:
            name: V
            description: Volts
          gain:
            value: 2.0
            frequency: 0.0
          filter:
            type: ANALOG
          name: ' AMPLIFIER_FILTER'
          delay: 0.0
    extras:
      DBIRD_response_type: THEORETICAL
      Number_of_zeroes: '0'
      Number_of_poles: '0'
    notes:
      - '#   Diviseur par 1 en preampli'
      - 'Response_type                        : THEORETICAL   # optionel'
      - 'Input_unit                           : V             # optionel'
      - 'Output_unit                          : V             # optionel'
      - 'Transfer_normalization_frequency     : 0             # optionel'

nanometrics/TAURUS-G-04.response.yaml
-------------------------------------
.. code-block:: yaml

    ---
    format_version: '0.106'
    response:
      decimation_info:
        correction: true
      stages:
        - input_units:
            name: V
            description: Volts
          output_units:
            name: V
            description: Volts
          gain:
            value: 0.4
            frequency: 0.0
          filter:
            type: ANALOG
          name: ' AMPLIFIER_FILTER'
          delay: 0.0
    extras:
      DBIRD_response_type: THEORETICAL
      Number_of_zeroes: '0'
      Number_of_poles: '0'
    notes:
      - '#   Diviseur par 1 en preampli'
      - 'Response_type                        : THEORETICAL   # optionel'
      - 'Input_unit                           : V             # optionel'
      - 'Output_unit                          : V             # optionel'
      - 'Transfer_normalization_frequency     : 0             # optionel'


nanometrics/TAURUS.100.response.yaml
-------------------------------------
.. code-block:: yaml

    ---
    format_version: '0.106'
    response:
      decimation_info:
        correction: true
      stages:
        - input_units: &id001
            name: V
            description: Volts
          output_units: &id002
            name: counts
            description: Digital Counts
          gain:
            value: 1000012.875
            frequency: 0.0
          filter:
            type: AD_CONVERSION
            input_full_scale: "16.777"
            output_full_scale: "16777216.000000"
          name: ' DIGITIZER'
          input_sample_rate: 0.0
          output_sample_rate: 30000.000300000003
          delay: 0.0
        - input_units: *id002
          output_units: *id002
          filter:
            $ref: include/tau_FirSym2_s1_100.filter.yaml#filter
          name: ' DECIMATION'
          input_sample_rate: 30000.000300000003
          output_sample_rate: 2000.0
          delay: 0.0
          decimation_factor: 15
        - input_units: *id002
          output_units: *id002
          filter:
            $ref: include/tau_FirSym2_s2_100.filter.yaml#filter
          name: ' DECIMATION'
          input_sample_rate: 2000.0
          output_sample_rate: 200.0
          delay: 0.0
          decimation_factor: 10
        - input_units: *id002
          output_units: *id002
          filter:
            $ref: include/tau_FirSym2_s3_100.filter.yaml#filter
          name: ' DECIMATION'
          input_sample_rate: 200.0
          output_sample_rate: 100.0
          delay: 0.0
          decimation_factor: 2
    extras:
      DBIRD_response_type: "THEORETICAL"
      Output_sampling_interval: '1.0000000e-02'
    notes:
      - '# Generated by G. Cougoulat LGIT '
      - '# On: 10/25/2010'
      - "Response_name                        : DIGITIZER# optionnel"
      - "Response_type                        : THEORETICAL# optionnel"
      - "Input_sampling_interval              : 0# optionnel "
      - "Output_sampling_interval             : 3.3333333e-05# (= 5.10^-4 / 15)  "
      - "Input_full_scale                     : 16.777# 16.777 Vpp , gain = 1"
      - "Output_full_scale                    : 16777216.000000# 2^24 "
      - "Sensitivity                          : 1000012.875# 1\xB5v"
      - '# stage 1 decimation par 15'
      - "Response_name                        : DECIMATION# optionnel"
      - "Response_type                        : THEORETICAL# optionnel"
      - "Input_sampling_interval              : 3.3333333e-05# 3.0000000e-04# ~ 30000\
        \ / 15 = "
      - "Output_sampling_interval             : 5.0000000e-04# 2.0000000e-03# optionnel"
      - '# stage 2 decimation par 10'
      - "Response_name                        : DECIMATION# optionnel"
      - "Response_type                        : THEORETICAL# optionnel"
      - "Input_sampling_interval              : 5.0000000e-04# 2.0000000e-03"
      - "Output_sampling_interval             : 5.0000000e-03# 2.0000000e-02# optionnel"
      - '# stage 3 decimation par 2'
      - "Response_name                        : DECIMATION# optionnel"
      - "Response_type                        : THEORETICAL# optionnel"
      - "Input_sampling_interval              : 5.0000000e-03#2.0000000e-02"
      - "Output_sampling_interval             : 1.0000000e-02# optionnel"

nanometrics/TAURUS.200.response.yaml
-------------------------------------
.. code-block:: yaml

    ---
    format_version: '0.106'
    response:
      decimation_info:
        correction: true
      stages:
        - input_units: &id001
            name: V
            description: Volts
          output_units: &id002
            name: counts
            description: Digital Counts
          gain:
            value: 1048576.0
            frequency: 0.0
          filter:
            type: AD_CONVERSION
            input_full_scale: "16.0"
            output_full_scale: "16777216.000000"
          name: ' DIGITIZER'
          input_sample_rate: 0.0
          output_sample_rate: 30000.000300000003
          delay: 0.0
        - input_units: *id002
          output_units: *id002
          filter:
            $ref: include/tau_FirSym2_s1_200.filter.yaml#filter
          name: ' DECIMATION'
          input_sample_rate: 30000.000300000003
          output_sample_rate: 2000.0
          delay: 0.0
          decimation_factor: 15
        - input_units: *id002
          output_units: *id002
          filter:
            $ref: include/tau_FirSym2_s2_200.filter.yaml#filter
          name: ' DECIMATION'
          input_sample_rate: 2000.0
          output_sample_rate: 400.0
          delay: 0.0
          decimation_factor: 5
        - input_units: *id002
          output_units: *id002
          filter:
            $ref: include/tau_FirSym2_s3_200.filter.yaml#filter
          name: ' DECIMATION'
          input_sample_rate: 400.0
          output_sample_rate: 200.0
          delay: 0.0
          decimation_factor: 2
    extras:
      DBIRD_response_type: "THEORETICAL"
      Output_sampling_interval: '5.0000000e-03'
    notes:
      - '# Generated by G. Cougoulat LGIT '
      - '# On: 10/25/2010'
      - "Response_name                        : DIGITIZER# optionnel"
      - "Response_type                        : THEORETICAL# optionnel"
      - "Input_sampling_interval              : 0# optionnel "
      - "Output_sampling_interval             : 3.3333333e-05# (= 5.10^-4 / 15)  "
      - "Input_full_scale                     : 16.0# 16 Vpp , gain = 1"
      - "Output_full_scale                    : 16777216.000000# 2^24 "
      - "Sensitivity                          : 1048576.0# 1\xB5v"
      - '# stage 1 decimation par 15'
      - "Response_name                        : DECIMATION# optionnel"
      - "Response_type                        : THEORETICAL# optionnel"
      - "Input_sampling_interval              : 3.3333333e-05# 3.0000000e-04# ~ 30000\
        \ / 15 = "
      - "Output_sampling_interval             : 5.0000000e-04# 2.0000000e-03# optionnel"
      - '# stage 2 decimation par 5'
      - "Response_name                        : DECIMATION# optionnel"
      - "Response_type                        : THEORETICAL# optionnel"
      - "Input_sampling_interval              : 5.0000000e-04# 2.0000000e-03"
      - "Output_sampling_interval             : 2.5000000e-03# 2.0000000e-02# optionnel"
      - '# stage 3 decimation par 2'
      - "Response_name                        : DECIMATION# optionnel"
      - "Response_type                        : THEORETICAL# optionnel"
      - "Input_sampling_interval              : 2.5000000e-03#2.0000000e-02"
      - "Output_sampling_interval             : 5.0000000e-03# optionnel"


In ``obsinfo``, we would have 2 choices for implementing the two choices
here: sampling rate and gain:
1) put the gain into a "preamplifier" file and the "sampling rate" into a
   ``datalogger`` file
2) put both in a ``datalogger`` file

The second is more complicated but also easier to directly translate from AROL
and expandable to other configuration dimensions such as the choice between
minimum phase and linear phase filtering, or the implementation of a DC
removal filter.  We show this option below.

First, we would break the different ``stages`` into their own files:

tau_DIGITIZER.100.stage.yaml
.. code-block:: yaml

    ---
    format_version: '0.110'
    stage:
        - input_units: &id001
            name: V
            description: Volts
          output_units: &id002
            name: counts
            description: Digital Counts
          gain:
            value: 1000012.875
            frequency: 0.0
          filter:
            type: AD_CONVERSION
            input_full_scale: "16.777"
            output_full_scale: "16777216.000000"
          name: ' DIGITIZER'
          input_sample_rate: 0.0
          output_sample_rate: 30000.000300000003
          delay: 0.0

tau_DIGITIZER.200.stage.yaml
.. code-block:: yaml

    ---
    format_version: '0.110'
    stage:
        - input_units: &id001
            name: V
            description: Volts
          output_units: &id002
            name: counts
            description: Digital Counts
          gain:
            value: 1000012.875
            frequency: 0.0
          filter:
            type: AD_CONVERSION
            input_full_scale: "16.777"
            output_full_scale: "16777216.000000"
          name: ' DIGITIZER'
          input_sample_rate: 0.0
          output_sample_rate: 30000.000300000003
          delay: 0.0

tau_FirSym2_s1_100.stage.yaml
.. code-block:: yaml

    ---
    format_version: '0.110'
    stage:
        - input_units: *id002
            name: counts
            description: Digital Counts
          output_units: *id002
            name: counts
            description: Digital Counts
          filter:
            $ref: include/tau_FirSym2_s1_100.filter.yaml#filter
          name: ' DECIMATION'
          delay: 0.0
          decimation_factor: 15

tau_FirSym2_s2_100.stage.yaml
.. code-block:: yaml

    ---
    format_version: '0.110'
    stage:
        - input_units: *id002
            name: counts
            description: Digital Counts
          output_units: *id002
            name: counts
            description: Digital Counts
          filter:
            $ref: include/tau_FirSym2_s2_100.filter.yaml#filter
          name: ' DECIMATION'
          delay: 0.0
          decimation_factor: 10

tau_FirSym2_s3_100.stage.yaml
.. code-block:: yaml

    ---
    format_version: '0.110'
    stage:
        - input_units: *id002
            name: counts
            description: Digital Counts
          output_units: *id002
            name: counts
            description: Digital Counts
          filter:
            $ref: include/tau_FirSym2_s3_100.filter.yaml#filter
          name: ' DECIMATION'
          delay: 0.0
          decimation_factor: 2

Notice that we lost the sampling rates and had to specify the units
in each file rather than using YAML anchors.  Here is the modified
100 sps datalogger file:

.. code-block:: yaml

    ---
    format_version: '0.110'
    datalogger:
      sample_rate: 100
      correction: true
      stages:
        - {$ref: 'tau_DIGITIZER.stage.yaml#stage'}
        - {$ref: 'tau_FirSym2_s1.stage.yaml#stage'}
        - {$ref: 'tau_FirSym2_s2.stage.yaml#stage'}
        - {$ref: 'tau_FirSym2_s3.stage.yaml#stage'}
        
This file is ALMOST AROL-compatible, except that

- the output ``sample_rate`` is specified
- `response` is renamed ``datalogger`` and can include ``equipment`` (where
  is this in AROL?)
- `stages` is renamed `stages` (change in v0.111?).
- `correction` is not under `decimation_info` (change in v0.111?)

Many of these differences come from our "flattening" of the StationXML `Response`,
should we move back for compatibility?

We can add one of the gains as well, here before the digitizer:

.. code-block:: yaml

    ---
    format_version: '0.110'
    datalogger:
      sample_rate: 100
      correction: true
      stages:
        - {$ref: "TAURUS-G-1.response.yaml#response/stage"}
        - {$ref: 'tau_DIGITIZER.stage.yaml#stage'}
        - {$ref: 'tau_DIGITIZER.stage.yaml#stage'}
        - {$ref: 'tau_FirSym2_s1.stage.yaml#stage'}
        - {$ref: 'tau_FirSym2_s2.stage.yaml#stage'}
        - {$ref: 'tau_FirSym2_s3.stage.yaml#stage'}

Now, putting configurations inside the ``Datalogger`` file we have:

.. code-block:: yaml

    ---
    format_version: '0.110'
    datalogger:
      sample_rate: 100
      correction: true
      stages:
        - {$ref: "TAURUS-G-1.response.yaml#response/stage"}
        - {$ref: 'tau_DIGITIZER.stage.yaml#stage'}
        - {$ref: 'tau_DIGITIZER.stage.yaml#stage'}
        - {$ref: 'tau_FirSym2_s1.stage.yaml#stage'}
        - {$ref: 'tau_FirSym2_s2.stage.yaml#stage'}
        - {$ref: 'tau_FirSym2_s3.stage.yaml#stage'}
      configuration_default: '100sps_G1'
      configuration_definitions:
        "100sps_G1":
            configuration_description: "100 sps, gain=1"
        "100sps_G2":
            configuration_description: "100 sps, gain=2"
            stages:
              - {$ref: "TAURUS-G-2.response.yaml#response/stage"}
              - {$ref: 'tau_DIGITIZER.100.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s1_100.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s2_100.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s3_100.stage.yaml#stage'}
        "100sps_G04":
             configuration_description: "100 sps, gain=0.4"
             stages:
              - {$ref: "TAURUS-G-04.response.yaml#response/stage"}
              - {$ref: 'tau_DIGITIZER.100.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s1_100.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s2_100.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s3_100.stage.yaml#stage'}
        "200sps_G1":
            sample_rate: 200
            configuration_description: "200 sps, gain=1"
             stages:
              - {$ref: "TAURUS-G-1.response.yaml#response/stage"}
              - {$ref: 'tau_DIGITIZER.200.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s1_200.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s2_200.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s3_200.stage.yaml#stage'}
        "200sps_G2":
            sample_rate: 200
            configuration_description: "200 sps, gain=2"
             stages:
              - {$ref: "TAURUS-G-2.response.yaml#response/stage"}
              - {$ref: 'tau_DIGITIZER.200.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s1_200.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s2_200.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s3_200.stage.yaml#stage'}
        "200sps_G04":
            sample_rate: 200
            configuration_description: "200 sps, gain=0.4"
             stages:
              - {$ref: "TAURUS-G-04.response.yaml#response/stage"}
              - {$ref: 'tau_DIGITIZER.200.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s1_200.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s2_200.stage.yaml#stage'}
              - {$ref: 'tau_FirSym2_s3_200.stage.yaml#stage'}
   extras:
      DBIRD_response_type: "THEORETICAL"
      Output_sampling_interval: '1.0000000e-02'

This is not the best example, as the Taurus appears to use different FIRs
for each sampling rate: in many cases we can further reduce repetion by giving
the same FIRs for different sampling rates, simply adding FIRs to account for
the different sampling rates (e.g. CS5321 converters) or having a different
sampling rate for the first stage (e.g. AD128* converters).  One of the advantages
is that we only specify a given FIR (or IIR) sequence once in the hierarchy

Converting obsinfo files to AROL
=============================================
This should be fairly straightforward via a program that "explodes" an 
InstrumentCompoennt file into individual files and an AROL-style configuration
file.

Converting AROL files to obsinfo
=============================================
It may be possible to write
a routine that combines a given set of files into an obsinfo file with different
configurations, although there would be much repetition in this file.

Directly using AROL files in obsinfo
=============================================
For now, this is not directly possible because of the lack of configuration
information in the AROL InstrumentComponent files.  See below for converting
AROL files to obsinfo

Another, more complicated option would be to allow obsinfo to read AROL-style
configuration files (``$arol`` operator instead of ``$ref``?), but the top-level
configuration would have to allow several fields for the configuration

