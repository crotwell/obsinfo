*****************
Overview
*****************

Information files are the core units of the obsinfo system.  We present here
the most important concepts, the hierarchy of information files and some
examples.

-----------------
Concepts
-----------------

- The basic hierarchy is `subnetwork` -> `stations` -> `instrumentation`
  -> `channels` -> {`datalogger`, `preamplifier`, `sensor`} -> `stages`
  -> `filter`
- `datalogger`, `preamplifier` and `sensor` objects:
    - are at the same level and all have `stages` subobjects.
    - Are refered to collectively as`instrument_compoents`
    - Their `stages` are put into StationXML from top (stage N) to bottom
      (stage N+M).  The ordering is `sensor` -> `preamplifier` -> `datalogger`,
      so the top `sensor` stage will be the instrument's stage 0.
    - `preamplifier` is optional.
- information is generally divided into "atomic" files which are referenced
  using the `$ref:` operator (inherited from `JSONref <http://jsonref.org>`_).
- The paths in the $ref operator are added to the `datapath` specified in the
  `.obsinforc` file (often the current directory, a local instrumentation
  database and possibly an online database)
- **base objects and configuration** (:ref:`Details <base_configuration>`)
    - allow partial (pre-configured) customization of `station`,
      `instrumentation` and `instrument_component` objects
    - are specified using a `base` and (optional) `config` object at each of
      these levels in the information files.
    - Use `configuration_definitions` (and a `configuration_default` object
      in case `config` is not specified) sub-objects specified within the `base`
      configurable object
- **channel modifications** (:ref:`Details <channel_modifications>`)
    - allow full customization of all objects
    - provide a "base" customization for all channels, then a
      specification/customization of differences between individual channels
- The order of evaluation for customization is `base` < `config` <
  `channel_modifications`. Within `channel_modifications`, the most specific
  channel specifiers override the more general ones.

-----------------
Hierarchy
-----------------

We will present:
 
1) A basic level diagram of the entire structure (missing many sub-fields)
2) A full decription of each atomic-level object
3) A full level diagram of the entire structure, including all sub-fields

The definition (`JSON Schemas <https://json-schema.org>`_) for the information files are found
in `obsinfo/data/schemas`

1) Basic level diagram
===================================================

Only required fields are shown, except a few very commonly-used fields,
which are prefixed by "*". Atomic objects described below are surrounded by <>

.. code-block:: yaml

    subnetwork:
        operators:
            - <operator>
        network: <network>
        stations:
            {STATIONNAME1}: <station>
                site: <string>
                start_date: <string>
                end_date: <string>
                location_code: <string>
                locations:
                    {LOCATION_1}: <location>
                    {LOCATION_2}: <location>
                    ...
                instrumentation: <instrumentation>
                    base: <instrumentation_base>
                        equipment: <equipment>
                        channels:
                            default: <channel>
                                datalogger: 
                                    base: <datalogger_base>
                                        equipment: <equipment>
                                        sample_rate: <number>
                                        stages:
                                            - 
                                                base: <stage_base>
                                                    input_units:
                                                        name: <string>
                                                        description: <string>
                                                    output_units:
                                                        name: <string>
                                                        description: <string>
                                                    gain:
                                                        value: <number>
                                                    filter: <filter>
                                                        type: <string>
                                            - base: <stage>
                                            - ...
                                *preamplifier: <preamplifier>
                                    base:
                                        equipment:
                                            <equipment>
                                        stages:
                                            - base: <stage>
                                            - base: <stage>
                                            - ...
                                sensor: <sensor>
                                    base:
                                        equipment:
                                            <equipment>
                                        seed_codes: 
                                            band_base: "B" or "S"
                                            instrument: <single character>
                                        stages:
                                            - base: <stage>    
                                            - base: <stage>
                                            - ...
                           {SPECIFIC-CHANNEL1}: subset of <channel>
                           {SPECIFIC-CHANNEL2}: subset of <channel>
                           ...
            {STATIONNAME2}:
                ...


2) Atomic level diagram with comments
===================================================

Starred fields are optional. If you put a level in a separate file, it is good
practice to use the following file structure:

.. code-block:: yaml

    format_version: <format_version>
    *revision: <revision>
    *notes: <list of strings>
    level: <level>
    
Major objects
************************
        
``subnetwork``
------------------------

.. code-block:: yaml

    network: <network>
    operators: <list of operator>
    stations:
        <STATIONNAME1>:
            <station>
        <STATIONNAME2>:
            <station>
        ...
    *reference_names:
        campaign: <string>
        operator: <string>
    *comments: list of strings
    *extras: <free-form object>

``network``
------------------------

.. code-block:: yaml

    code: <string>
    name: <string>
    start_date: <date-formatted string>
    end_date: <date-formatted string>
    description: <string>
    *operators: <list of operator>
    *comments: <list of <string> and/or <stationxml_comment>>
    *restricted_status: 'open', 'closed', or 'partial'
    *source_id: <uri-formatted string>
    *identifiers: <list of uri-formatted string, must have prefix>

``station``
------------------------

.. code-block:: yaml

    site: <string>
    start_date: <string>
    end_date: <string>
    location_code: <string>
    locations: object list of <location>
    instrumentation:
        base: <instrumentation_base>
        *configuration: <string>
        *modifications: <modifications>
        *channel_modifications: {}
        *serial_number: <string>
        *notes: <list of strings>
    *operators: <operators>
    *comments: list of strings or <stationxml_comment>
    *extras: <free-form object>
    *processing:
        - *clock_correction_linear: {}
        - *clock_correction_leapsecond: {}
    *restricted_status: ['open', 'closed', 'partial', or 'unknown']
    *source_id: <string in uri format>
    *external_references:
        - uri: <string in uri format>
          description: <string>
    *identifiers:
        - <string in uri format>
    *water_level: <number>
    *notes: <list of strings>

``instrumentation_base``
------------------------

.. code-block:: yaml

    equipment: {}
    channels:
        default:
            <channel>
       <SPECIFIC-CHANNEL1>:
            subset of <channel>
       <SPECIFIC-CHANNEL2>:
            subset of <channel>
       ...
    *configuration_default: <string>
    *configurations:
        {CONFIGURATION_1}: <subset of instrumentation>
        {CONFIGURATION_2}: <subset of instrumentation>
        ...

``channel``
------------------------

.. code-block:: yaml

    datalogger:
        base: <datalogger>
        *configuration: <string>
    sensor:
        base: <sensor>
        *configuration: string
    *preamplifier:*
        base: <preamplifier>
        *configuration: <string>
    *orientation: <orientation>
    *location_code: <string>   # if not specified, inherits from station
    *comments: <list of string>
    *restricted_status": 'open', 'closed'
    *source_id": <uri-formatted string>
    *identifiers": <list of uri-formatted strings, must have scheme>
    *external_references": <list of {uri: description}>
    *extras: <free-format object>

``datalogger_base``
------------------------

.. code-block:: yaml

    <GENERIC_COMPONENT>
    sample_rate: number
    *correction: number


``preamplifier_base``
------------------------

.. code-block:: yaml

    <GENERIC_COMPONENT>


``sensor_base``
------------------------

.. code-block:: yaml

        <GENERIC_COMPONENT>
         seed_codes:

``GENERIC_COMPONENT`` 
------------------------

.. code-block:: yaml

    equipment:  <equipment>
    *stage_modifications: {}
    *notes: <list of string>
    *stages:
        - base: <stage>
          *configuration: <string>
        - base: <stage>
          *configuration: <string>
        - ...
    *configuration_default: <string>
    *configurations:
        {CONFIGURATION_1}: <subset of datalogger, sensor, or preamplifier>
        {CONFIGURATION_2}: <subset of datalogger, sensor, or preamplifier>
        ...

``stage_base``
------------------------

.. code-block:: yaml

    input_units: <string>
    output_units: <string>
    gain: <float>
    *name: <string>
    *description: <string>
    *decimation_factor: <integer>
    *delay: <number>
    *calibration_date: <string>
    *polarity: '+' or '-'     # default is '+'
    *input_sample_rate: <number>
    *resource_id: <string>
    *filter:
        <filter>
    *configuration_default: <string>
    *configurations:
        {CONFIGURATION_1}: <subset of stage>
        {CONFIGURATION_2}: <subset of stage>
        ...

``filter``
------------------------

fields depend on type:

.. code-block:: yaml

    type: "PoleZeros"
    poles: <list of string>
    zeros: <list of string>
    *delay.samples: <float>
    *transfer_function_type:  "LAPLACE (RADIANS/SECOND)", "LAPLACE (HERTZ)", or "DIGITAL (Z-TRANSFORM)"
    *normalization_frequency: <number>
    *normalization_factor: <number>
    *resource_id: <string>

.. code-block:: yaml

    type: "FIR"
    symmetry:  "EVEN", "ODD" or "NONE"
    coefficients:  <list>
    coefficient_divisor: <number>
    *delay.samples: <number>
    *resource_id: <string>

.. code-block:: yaml

    type: "Coefficients"
    numerator_coefficients: <list>
    denominator_coefficients": <list>
    *delay.samples: <number>
    *transfer_function_type: "ANALOG (RADIANS/SECOND)", "ANALOG (HERTZ)" or "DIGITAL"*
    *offset: <number>
    *resource_id: <string>

.. code-block:: yaml

    type: "ResponseList"
    elements: <list>
    *delay.samples: <number>
    *resource_id: <string>

.. code-block:: yaml

    type: "Polynomial"
    frequency_lower_bound: <number>
    frequency_upper_bound: <number>
    approximation_lower_bound: <number>
    approximation_upper_bound: <number>
    maximum_error: <number>
    coefficients: <list of number>
    *approximation_type: "MACLAURIN"
    *resource_id: <string>

.. code-block:: yaml

    type: "ADConversion"
    input_full_scale: <number>
    output_full_scale: <number>
    *delay.samples: <number>
    *resource_id: <string>

.. code-block:: yaml

    type: "Analog"
    *delay.seconds: <number>
    *resource_id: <string>
 
.. code-block:: yaml

    type: "Digital"
    *delay.samples: <number>
    *resource_id: <string>

Minor objects
************************

person_, operator_, location_base_ and network_ are often
in separate files.

equipment_ is widespread enough that it should probably have its
own schema file

person:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    names: <list of string>
    *agencies: <list of string>
    *emails: <list of string>
    *phones: <list of string>
        
equipment:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    type: <string>
    description: <string>
    manufacturer: <string>
    model: <string>
    *vendor: <string>
    *serial_number: <string>
    *installation_date: <date-formatted string>
    *removal_date: <date-formatted string>
    *calibration_dates: <list of date_formatted strings>
    *resource_id: <'GENERATOR:Meaningful ID' str>

location:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    base:
        uncertainties.m:
            lat: <number> (in meters)
            lon: <number> (in meters)
            elev: <number> (in meters)
        depth.m: <number>
        geology: <string>
        vault: <string>
        *localisation_method: <string>
    position:
        lat: <number> (in degrees)
        lon: <number> (in degrees)
        elev: <number> (in meters)
        

operator:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    agency: <string>
    *contacts: <list of person>
    *website: <string>

revision:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    date: <string>
    authors: <list of person>

stationxml_comment:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    value: <string>
    *begin_effective_time: <date-formatted string>
    *end_effective_time: <date-formatted string>
    *authors: <list of person>


3) Full level diagram
===================================================

Structural units
************************

A full ``obsinfo`` subnetwork description consists of the following fields
(starred fields are optional):

.. code-block:: yaml

    format_version: {}
    *revision: {}
    *notes: []
    subnetwork:
        network: <network>
        operators: <list of operator>
        *restricted_state: {}
        *comments: <list of string>
        *extras: <free-form object>
        *reference_names:
            campaign: <string>
            operator: <string>
        stations:
            <STATIONNAME1>:
                site: <string>
                start_date: <string>
                end_date: <string>
                location_code: <string>
                *serial_number: <string>
                *operators: <list of operator>
                instrumentation:
                    base: 
                        equipment: <equipment>
                        channels:
                            default:
                                *orientation: <orientation>
                                datalogger:
                                    base:
                                        << GENERIC_COMPONENT
                                        sample_rate: <number>
                                        *correction: <number>
                                    *configuration: <string>
                                    *modifications: <subset of base>
                                    *stage_modifications: <stage_modifications>
                                    *serial_number: <string>
                                    *notes: <list of string>
                                *preamplifier:*
                                    base: 
                                        << GENERIC_COMPONENT
                                    *configuration: <string>
                                    *modifications: <subset of base>
                                    *stage_modifications: <stage_modifications>
                                    *serial_number: <string>
                                    *notes: <list of string>
                                sensor:
                                    base:
                                        << GENERIC_COMPONENT
                                        seed_codes:
                                            band_base: 'B' or 'S'
                                            instrument: <character>
                                    *configuration: <string>
                                    *modifications: <subset of base>
                                    *stage_modifications: <stage_modifications>
                                    *serial_number: <string>
                                    *notes: <list of string>
                                *location_code: <string>
                                *restricted_status: 'open', 'closed', or 'partial'
                                *source_id: <uri-formatted string>
                                *identifiers: <list of uri-formatted strings>
                                *external_references:
                                    - uri: <uri-formatted string>
                                      description: <string>
                                *comments: <list of string or stationxml_comment>
                                *extras: <free-form object>
                           <SPECIFIC-CHANNEL1>: {}
                           <SPECIFIC-CHANNEL2>: {}
                           ...
                    *channel_modifications: {}
                    *serial_number: <string>
                locations: {}
                *notes: <list of string>
                *comments: <list of string or stationxml_comment>
                *extras: <free-form object>
                *processing:
                    - *clock_correction_linear: {}
                    - *clock_correction_leapsecond: {}
                *water_level.m: <number>
                *restricted_status: 'open', 'closed', or 'partial'
                *source_id: <uri-formatted string>
                *identifiers: <list of uri-formatted strings>
                *external_references:
                    - uri: <uri-formatted string>
                      description: <string>
            <STATIONNAME2>:
                ...
