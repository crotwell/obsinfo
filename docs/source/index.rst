.. obsinfo-test documentation master file, created by
   sphinx-quickstart on Mon Jul 19 11:50:58 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Obsinfo documentation
========================================


.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
  :glob:
  
  overview
  installation
  execution
  tutorial
  information_files
  datacite
  advanced
  nomenclature
  training_course
  developers
  modules
  changelog
  

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
