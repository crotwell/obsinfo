*******************************
Nomenclature
*******************************

Terms that have specific meanings in the **obsinfo** universe

experiment
  the highest level of the experiment-campaign-expedition sequence, it represents
  all data collected at one region (something like an FDSN network)
  
campaign
  A data collection campaign, which may consist of one or more expeditions.
  Each campaign generally represents one group of data that will be sent to
  a data center
  
expedition
  One data-collection mission, generally a ship leg for OBS deployments
  
element
  Anything written as a key in a ``key: value`` pair in an information file

key
    Must be a string

attribute

parameter

  
