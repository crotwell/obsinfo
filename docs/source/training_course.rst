***************
Training Course
***************

.. toctree::
  :caption:  Table of contents
  :maxdepth: 1

  training_course/1_introduction
  training_course/2_setup
  training_course/3_create_your_own
  training_course/4_advanced
  training_course/5_future
  