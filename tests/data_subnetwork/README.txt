Each of the yaml class snippets here should include every field possible.

Should I also make one using only required fields (_required) and one lacking
a required field (_error) to make sure that they work / don't work as expected?