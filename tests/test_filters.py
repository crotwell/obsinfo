#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test instrumnetation, stages and filter classes
"""
import warnings
from pathlib import Path
import unittest

import yaml
import obspy.core.inventory.response as op_resp
import obspy.core.inventory.util as op_util

# obsinfo modules
from obsinfo.instrumentation import (Filter, Coefficients, FIR, PolesZeros,
                                     ResponseList, Polynomial, ADConversion,
                                     Analog, Digital)
from obsinfo.obsmetadata import ObsMetadata

warnings.simplefilter("once")
warnings.filterwarnings("ignore", category=DeprecationWarning)
verbose = False


class FilterTest(unittest.TestCase):
    """
    Test elements in the filter/ directory

    Attributes:
        testing_path (str): path to datafiles to be tested aside from the
            examples
    """

    def setUp(self):
        """
        Set up default values and paths
        """
        self.testing_path = Path(__file__).parent.joinpath("data_filters")

    def read_yaml_root(self, filename):
        with open(str(self.testing_path / filename), 'r') as f:
            return yaml.safe_load(f)

    def _test_against_schema(self, root, schema_file, sublevels):
        """Test an infodict against a schema
        
        Args:
            root (dict or :class:`ObsMetadata`): the infodict
            schema_file (str): the schema filename (w/o '.schema.json')
            sublevels (list): sublevels to traverse to get to proper
                comparison level
        """
        if '.schema.json' not in schema_file:
            schema_file += '.schema.json' 
        schema = ObsMetadata._read_schema_file(schema_file)[0]
        schema_name = schema_file + '#'
        for level in sublevels:
            if level not in schema:
                raise ValueError(f'{schema_name} has no "{level}" sublevel')
            schema = schema[level]
            schema_name += f'{level}/'
        self.assertTrue(ObsMetadata()._report_errors(root, schema),
                        f'file does not match schema {schema_name}')

    def test_poles_zeros(self):
        """Test reading and converting a PolesZeros object"""
        root = self.read_yaml_root('poles_zeros.yaml')
        self._test_against_schema(root,'filter',['definitions','POLESZEROS'])
        obj = Filter.construct(root, 0, 'test')
        print(obj)  # Only prints if something fails
        self.assertIsInstance(obj, PolesZeros)
        self.assertEqual(obj.delay_seconds, 0.001)
        self.assertEqual(len(obj.poles), 7)

    def test_FIR(self):
        """Test reading and converting a FIR object"""
        root = self.read_yaml_root('FIR.yaml')
        self._test_against_schema(root,'filter',['definitions','FIR'])
        obj = Filter.construct(root, 0, 'test')
        print(obj)  # Only prints if something fails
        self.assertIsInstance(obj, FIR)
        self.assertEqual(obj.delay_samples, 5.)
        self.assertEqual(len(obj.coefficients), 11)

    def test_Coefficients(self):
        """Test reading and converting a Coefficients object"""
        root = self.read_yaml_root('coefficients.yaml')
        self._test_against_schema(root,'filter',['definitions','COEFFICIENTS'])
        obj = Filter.construct(root, 0, 'test')
        print(obj)  # Only prints if something fails
        self.assertIsInstance(obj, Coefficients)
        self.assertEqual(obj.delay_samples, 5.)
        self.assertEqual(len(obj.numerator_coefficients), 11)
        self.assertEqual(len(obj.denominator_coefficients), 11)

    def test_ResponseList(self):
        """Test reading and converting a ResponseList object"""
        root = self.read_yaml_root('response_list.yaml')
        self._test_against_schema(root,'filter',['definitions','RESPONSELIST'])
        obj = Filter.construct(root, 0, 'test')
        print(obj)  # Only prints if something fails
        self.assertIsInstance(obj, ResponseList)
        self.assertIsNone(obj.delay_seconds)
        self.assertEqual(len(obj.elements), 6)

    def test_Polynomial(self):
        """Test reading and converting a Polynomial object"""
        root = self.read_yaml_root('polynomial.yaml')
        self._test_against_schema(root,'filter',['definitions','POLYNOMIAL'])
        obj = Filter.construct(root, 0, 'test')
        print(obj)  # Only prints if something fails
        self.assertIsInstance(obj, Polynomial)
        self.assertEqual(len(obj.coefficients), 11)
        obj = Polynomial(root)
        print(obj)  # Only prints if something fails
        self.assertIsInstance(obj, Polynomial)
        self.assertEqual(len(obj.coefficients), 11)

    def test_ADConversion(self):
        """Test reading and converting an ADConversion object"""
        root = self.read_yaml_root('AD_conversion.yaml')
        self._test_against_schema(root,'filter',['definitions','ADCONVERSION'])
        obj = Filter.construct(root, 0, 'test')
        print(obj)  # Only prints if something fails
        self.assertIsInstance(obj, ADConversion)
        self.assertEqual(obj.delay_samples, None)
        self.assertEqual(obj.input_full_scale, 9.)
        self.assertEqual(len(obj.numerator_coefficients), 1)
        self.assertEqual(obj.numerator_coefficients[0], 1.)
        self.assertEqual(len(obj.denominator_coefficients), 0)

    def test_Analog(self):
        """Test reading and converting an Analog object"""
        root = self.read_yaml_root('analog.yaml')
        self._test_against_schema(root,'filter',['definitions','ANALOG'])
        obj = Filter.construct(root, 0, 'test')
        print(obj)  # Only prints if something fails
        self.assertIsInstance(obj, Analog)
        self.assertEqual(obj.delay_seconds, 0.)

    def test_Digital(self):
        """Test reading and converting an Digital object"""
        root = self.read_yaml_root('digital.yaml')
        self._test_against_schema(root,'filter',['definitions','DIGITAL'])
        obj = Filter.construct(root, 0, 'test')
        print(obj)  # Only prints if something fails
        self.assertIsInstance(obj, Digital)
        self.assertEqual(obj.delay_samples, 0.)
        self.assertEqual(len(obj.numerator_coefficients), 1)
        self.assertEqual(obj.numerator_coefficients[0], 1.)
        self.assertEqual(len(obj.denominator_coefficients), 0)


def suite():
    return unittest.makeSuite(FilterTest, 'test')


if __name__ == '__main__':
    unittest.main(defaultTest='suite')

