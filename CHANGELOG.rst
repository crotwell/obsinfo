CHANGELOG
===========

- 0.110.11:
    - Added obsinfo-makescripts-LC2SDS

- 0.110.12:
    - Fixed bug in clock correction reading (leapsecond and drift)
    - Made obsinfo-makescripts-LC2SDS write to campaign directory by default
    - Fixed some (not all) StationXML bugs:
        - Made clock correction Comments using JSON
        - Removed `restrictedStatus='unknown'` (invalid) from StationXML
        - Fixed bugs in Decimation filters and stage #s
        - Standardized uncertainties in Poles, Zeros, Elevation, Dip, Azimuths
    - Bugs that will probably have to wait for version 0.111:
        - instrumentation serial number and configurations not accounted for
- 0.110.13:
    - Made all obsinfo-test cases work
    - Added "serial_number" to station level (temporary fix before v 0.111)
    - Updated JSON schema to draft07, allowing more precise/compact information
      on mutiple-choice errors (e.g., different types of filter)
- 0.110.14:
    - Added StationXML test case and made it work
    - made ``python -m unittest discover`` work
- 0.110.15:
    - corrected JSON validation schema for ``orientation_code``
    - improved azimuth.deg and dip.deg schema definitions
    - Fixed a bug in reporting schema validation errors (in `obsmetadata.py`)
      introduced when shifting to draft07
    - Reduced a bug when a jsonref points to an inexistent pointer within
      a file
    - Uses Python 3.8-dependent syntax despite only requiring Python 3!
- 0.110.16:
    - Clean up information file validation, removing many redundancies in
      main/validate.py and hopefully fixing bug where schema files lacking
      ".schema.yaml" are searched for
    - Requires Python 3.7 syntax and only uses 3.7-dependent syntax
    - *post2*: streamlines `obsinfo-makescripts_LC2SDS`'s output
- 0.110.17:
    - Improvements to channel_modifications:
        - Make reading a new sensor, datalogger or preamplifier work
        - Enable shortcuts for entering serial number
        - Updated documentation
    - Added developer and information_file documentation
      in channel_modifications/
    - Moved tests up to united top-level directory
    - `offset` can be a non-integer
- 0.110.18:
    - Added datacite information file
    - Added schema and validation of datacite, author, location_base,
      network_info and operator information files
    - Cleaned up some information file validation glitches
      

